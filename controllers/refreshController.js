'use strict'

const asyncUtil = require('../services/asyncService');
const clashRoyaleApi = require('../api/clashRoyaleApi');
const clanTags = require('../config/clanTags');
const clanService = require('../services/clanService');
const arrayService = require('../services/arrayService');
const playerService = require('../services/playerService');

module.exports = {
    refreshClanData: asyncUtil(async (req, res, next) => {
        try {
            console.log('refreshClanData')
            let clanTag
            req.params.clanDivision ?
                clanTag = clanTags[(req.params.clanDivision).toLowerCase()] :
                clanTag = clanTags['a']
            let forceUpdate = true
            let warlogResponse = await clashRoyaleApi.getWarlog(clanTag, forceUpdate)
            let clanResponse = await clashRoyaleApi.getClan(clanTag, forceUpdate)
            let members = clanService.getMembers(clanResponse)
            for (const playerTag of members) {
                await clashRoyaleApi.getPlayer(playerTag, forceUpdate)
            }
            res.send('Refresh finished')
        } catch (err) {
            throw err
        }
    })
}