module.exports={
    CLASH_ROYALE_TOKEN: process.env.CLASH_ROYALE_TOKEN,
    RARITY: {
        COMMON_NAME: 'Common',
        RARE_NAME: 'Rare',
        EPIC_NAME: 'Epic',
        LEGENDARY_NAME: 'Legendary',
    },
    // Cost of level zero and one is 0 gold
    COST: {
        COMMON_COST: [0, 0, 5, 20, 50, 150, 400, 1000, 2000, 4000, 8000, 20000, 50000, 100000],
        RARE_COST:  [0, 0, 50, 150, 400, 1000, 2000, 4000, 8000, 20000, 50000, 100000],
        EPIC_COST:  [0, 0, 400, 1000, 2000, 4000, 8000, 20000, 50000, 100000],
        LEGENDARY_COST: [0, 0, 5000, 20000, 50000, 100000],
    }
}