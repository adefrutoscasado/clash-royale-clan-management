
const axios = require('axios');

const http = axios.create({
        headers: { 'Cache-Control': 'no-cache' },
        timeout: 100000
    }
)

let division = (process.argv[2]).toLowerCase()

async function refresh() {
    var url = `https://invernaliaroyale.herokuapp.com/invernalia/${division}/refresh`
    try {
        console.log('Launching scheduled refresh: ' + url)
        await http.get(url, {})
    } catch (err) {
        console.error('Error refreshing data or timeout')
        console.error(err.message)
    }
}

refresh()
