# Clash Royale Clan Stats

A web app to display stats about your clan, inluding:
- War stats: Win ratio, collected cards, overall efectiveness
- Battle stars: Win ratio, tie ratio, defeat ratio, 3-crown victories, total days playing
- Card level stats: Level and variance, spent gold.
- All current decks of the clan.

### Prerequisites

This app uses a number of open source projects to work properly:
- [Node.js](https://nodejs.org/)
- [Redis](https://redis.io/)

### Installing

Install Node modules:

```
npm install
```
Start the Redis server:

```
redis-server
```
Start the app:

```
npm start
```

## Deployment

It is possible to deploy on [Heroku](https://heroku.com/) easily. The recommended addons are:
- [Heroku Redis](https://elements.heroku.com/addons/heroku-redis): Data from Clash Royale official API is saved using Redis.
- [Heroku Scheduler](https://elements.heroku.com/addons/scheduler): A scheduler is needed in order to refresh the data from API in a given period of time.

## Demo

[https://invernaliaroyale.herokuapp.com/](https://invernaliaroyale.herokuapp.com/)


## License

This project is licensed under the MIT License
