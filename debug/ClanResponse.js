module.exports = 
{
    "tag": "PPYPCV",
    "name": "INVERNALIA",
    "description": "THE WINTER IS COMING! canteras INVERNALIA B,C,D,EyF Cada domingo subirán 2 y bajarán 2.Hay k participar siempre enla Guerra Clan",
    "type": "invite only",
    "score": 50559,
    "memberCount": 50,
    "requiredScore": 4600,
    "donations": 21990,
    "badge": {
        "name": "Freeze_01",
        "category": "01_Symbol",
        "id": 16000168,
        "image": "https://royaleapi.github.io/cr-api-assets/badges/Freeze_01.png"
    },
    "location": {
        "name": "Spain",
        "isCountry": true,
        "code": "ES"
    },
    "members": [
        {
            "name": "Adrián M",
            "tag": "CQ82YRR8",
            "rank": 1,
            "role": "elder",
            "expLevel": 13,
            "trophies": 6248,
            "donations": 10,
            "donationsReceived": 40,
            "donationsDelta": -30,
            "arena": {
                "name": "Grand Champion",
                "arena": "League 8",
                "arenaID": 20,
                "trophyLimit": 6100
            },
            "donationsPercent": 0.02
        },
        {
            "name": "er tabu",
            "tag": "U2YCLPQU",
            "rank": 2,
            "previousRank": 5,
            "role": "elder",
            "expLevel": 12,
            "trophies": 5241,
            "donations": 514,
            "donationsReceived": 440,
            "donationsDelta": 74,
            "arena": {
                "name": "Master II",
                "arena": "League 5",
                "arenaID": 17,
                "trophyLimit": 5200
            },
            "donationsPercent": 1.17
        },
        {
            "name": "Ragnar",
            "tag": "PYL8PPJ8",
            "rank": 3,
            "previousRank": 2,
            "role": "elder",
            "expLevel": 13,
            "trophies": 5212,
            "donations": 360,
            "donationsReceived": 480,
            "donationsDelta": -120,
            "arena": {
                "name": "Master II",
                "arena": "League 5",
                "arenaID": 17,
                "trophyLimit": 5200
            },
            "donationsPercent": 0.82
        },
        {
            "name": "GaliciaNoMapa",
            "tag": "22Q2RCJVV",
            "rank": 4,
            "previousRank": 7,
            "role": "coLeader",
            "expLevel": 13,
            "trophies": 5211,
            "donations": 872,
            "donationsReceived": 440,
            "donationsDelta": 432,
            "arena": {
                "name": "Master II",
                "arena": "League 5",
                "arenaID": 17,
                "trophyLimit": 5200
            },
            "donationsPercent": 1.98
        },
        {
            "name": "guyur",
            "tag": "8P9LV88Y",
            "rank": 5,
            "previousRank": 6,
            "role": "elder",
            "expLevel": 13,
            "trophies": 5200,
            "donations": 32,
            "donationsReceived": 120,
            "donationsDelta": -88,
            "arena": {
                "name": "Master II",
                "arena": "League 5",
                "arenaID": 17,
                "trophyLimit": 5200
            },
            "donationsPercent": 0.07
        },
        {
            "name": "SEIMUS",
            "tag": "2PR0999J",
            "rank": 6,
            "previousRank": 11,
            "role": "coLeader",
            "expLevel": 13,
            "trophies": 5118,
            "donations": 123,
            "donationsReceived": 560,
            "donationsDelta": -437,
            "arena": {
                "name": "Master I",
                "arena": "League 4",
                "arenaID": 16,
                "trophyLimit": 4900
            },
            "donationsPercent": 0.28
        },
        {
            "name": "vulosky",
            "tag": "90GP0RQ9",
            "rank": 7,
            "previousRank": 3,
            "role": "coLeader",
            "expLevel": 13,
            "trophies": 5100,
            "donations": 968,
            "donationsReceived": 440,
            "donationsDelta": 528,
            "arena": {
                "name": "Master I",
                "arena": "League 4",
                "arenaID": 16,
                "trophyLimit": 4900
            },
            "donationsPercent": 2.2
        },
        {
            "name": "Luke Boss",
            "tag": "L20PVCVC",
            "rank": 8,
            "previousRank": 14,
            "role": "elder",
            "expLevel": 12,
            "trophies": 5086,
            "donations": 576,
            "donationsReceived": 520,
            "donationsDelta": 56,
            "arena": {
                "name": "Master I",
                "arena": "League 4",
                "arenaID": 16,
                "trophyLimit": 4900
            },
            "donationsPercent": 1.31
        },
        {
            "name": "AustinSantoS",
            "tag": "22P0YP82",
            "rank": 9,
            "previousRank": 8,
            "role": "elder",
            "expLevel": 12,
            "trophies": 5067,
            "donations": 10,
            "donationsReceived": 320,
            "donationsDelta": -310,
            "arena": {
                "name": "Master I",
                "arena": "League 4",
                "arenaID": 16,
                "trophyLimit": 4900
            },
            "donationsPercent": 0.02
        },
        {
            "name": "#Núñez",
            "tag": "JJP9JUG2",
            "rank": 10,
            "previousRank": 10,
            "role": "elder",
            "expLevel": 12,
            "trophies": 5031,
            "donations": 693,
            "donationsReceived": 520,
            "donationsDelta": 173,
            "arena": {
                "name": "Master I",
                "arena": "League 4",
                "arenaID": 16,
                "trophyLimit": 4900
            },
            "donationsPercent": 1.58
        },
        {
            "name": "S.J.P",
            "tag": "G9J9CPL",
            "rank": 11,
            "previousRank": 12,
            "role": "coLeader",
            "expLevel": 13,
            "trophies": 5021,
            "donations": 498,
            "donationsReceived": 400,
            "donationsDelta": 98,
            "arena": {
                "name": "Master I",
                "arena": "League 4",
                "arenaID": 16,
                "trophyLimit": 4900
            },
            "donationsPercent": 1.13
        },
        {
            "name": "lebowsky",
            "tag": "9CCLRG9Y",
            "rank": 12,
            "previousRank": 4,
            "role": "coLeader",
            "expLevel": 13,
            "trophies": 4998,
            "donations": 640,
            "donationsReceived": 400,
            "donationsDelta": 240,
            "arena": {
                "name": "Master I",
                "arena": "League 4",
                "arenaID": 16,
                "trophyLimit": 4900
            },
            "donationsPercent": 1.46
        },
        {
            "name": "Maestre Sam",
            "tag": "2QGUUJV9",
            "rank": 13,
            "previousRank": 23,
            "role": "elder",
            "expLevel": 12,
            "trophies": 4966,
            "donations": 513,
            "donationsReceived": 520,
            "donationsDelta": -7,
            "arena": {
                "name": "Master I",
                "arena": "League 4",
                "arenaID": 16,
                "trophyLimit": 4900
            },
            "donationsPercent": 1.17
        },
        {
            "name": "Mikky",
            "tag": "8G8GCQRJ",
            "rank": 14,
            "previousRank": 17,
            "role": "elder",
            "expLevel": 12,
            "trophies": 4944,
            "donations": 436,
            "donationsReceived": 560,
            "donationsDelta": -124,
            "arena": {
                "name": "Master I",
                "arena": "League 4",
                "arenaID": 16,
                "trophyLimit": 4900
            },
            "donationsPercent": 0.99
        },
        {
            "name": "SeRmAo",
            "tag": "YLL2YG0Q",
            "rank": 15,
            "previousRank": 16,
            "role": "coLeader",
            "expLevel": 13,
            "trophies": 4935,
            "donations": 242,
            "donationsReceived": 360,
            "donationsDelta": -118,
            "arena": {
                "name": "Master I",
                "arena": "League 4",
                "arenaID": 16,
                "trophyLimit": 4900
            },
            "donationsPercent": 0.55
        },
        {
            "name": "★★★COROCOTA★★★",
            "tag": "GPCR09JL",
            "rank": 16,
            "previousRank": 13,
            "role": "coLeader",
            "expLevel": 13,
            "trophies": 4927,
            "donations": 1190,
            "donationsReceived": 560,
            "donationsDelta": 630,
            "arena": {
                "name": "Master I",
                "arena": "League 4",
                "arenaID": 16,
                "trophyLimit": 4900
            },
            "donationsPercent": 2.71
        },
        {
            "name": "sifh21",
            "tag": "2LLYC9PPC",
            "rank": 17,
            "previousRank": 34,
            "role": "elder",
            "expLevel": 12,
            "trophies": 4921,
            "donations": 385,
            "donationsReceived": 520,
            "donationsDelta": -135,
            "arena": {
                "name": "Master I",
                "arena": "League 4",
                "arenaID": 16,
                "trophyLimit": 4900
            },
            "donationsPercent": 0.88
        },
        {
            "name": "nawan",
            "tag": "2J8PGCPUC",
            "rank": 18,
            "previousRank": 9,
            "role": "elder",
            "expLevel": 13,
            "trophies": 4918,
            "donations": 513,
            "donationsReceived": 560,
            "donationsDelta": -47,
            "arena": {
                "name": "Master I",
                "arena": "League 4",
                "arenaID": 16,
                "trophyLimit": 4900
            },
            "donationsPercent": 1.17
        },
        {
            "name": "AaronusS",
            "tag": "2RPUJ9PPQ",
            "rank": 19,
            "previousRank": 19,
            "role": "elder",
            "expLevel": 12,
            "trophies": 4917,
            "donations": 254,
            "donationsReceived": 200,
            "donationsDelta": 54,
            "arena": {
                "name": "Master I",
                "arena": "League 4",
                "arenaID": 16,
                "trophyLimit": 4900
            },
            "donationsPercent": 0.58
        },
        {
            "name": "Asten",
            "tag": "88LQUVPG",
            "rank": 20,
            "previousRank": 20,
            "role": "coLeader",
            "expLevel": 13,
            "trophies": 4916,
            "donations": 632,
            "donationsReceived": 600,
            "donationsDelta": 32,
            "arena": {
                "name": "Master I",
                "arena": "League 4",
                "arenaID": 16,
                "trophyLimit": 4900
            },
            "donationsPercent": 1.44
        },
        {
            "name": "Jimlou",
            "tag": "QPCJQJQ",
            "rank": 21,
            "previousRank": 15,
            "role": "coLeader",
            "expLevel": 13,
            "trophies": 4916,
            "donations": 141,
            "donationsReceived": 600,
            "donationsDelta": -459,
            "arena": {
                "name": "Master I",
                "arena": "League 4",
                "arenaID": 16,
                "trophyLimit": 4900
            },
            "donationsPercent": 0.32
        },
        {
            "name": "PuxiWeed",
            "tag": "Y820L8LP",
            "rank": 22,
            "previousRank": 18,
            "role": "leader",
            "expLevel": 13,
            "trophies": 4916,
            "donations": 317,
            "donationsReceived": 560,
            "donationsDelta": -243,
            "arena": {
                "name": "Master I",
                "arena": "League 4",
                "arenaID": 16,
                "trophyLimit": 4900
            },
            "donationsPercent": 0.72
        },
        {
            "name": "Nayper",
            "tag": "U08RLC2U",
            "rank": 23,
            "previousRank": 24,
            "role": "coLeader",
            "expLevel": 12,
            "trophies": 4906,
            "donations": 216,
            "donationsReceived": 360,
            "donationsDelta": -144,
            "arena": {
                "name": "Master I",
                "arena": "League 4",
                "arenaID": 16,
                "trophyLimit": 4900
            },
            "donationsPercent": 0.49
        },
        {
            "name": "Maty97ro",
            "tag": "2QVQUCP",
            "rank": 24,
            "previousRank": 22,
            "role": "coLeader",
            "expLevel": 13,
            "trophies": 4904,
            "donations": 441,
            "donationsReceived": 400,
            "donationsDelta": 41,
            "arena": {
                "name": "Master I",
                "arena": "League 4",
                "arenaID": 16,
                "trophyLimit": 4900
            },
            "donationsPercent": 1
        },
        {
            "name": "joker",
            "tag": "2VC9GULRV",
            "rank": 25,
            "previousRank": 26,
            "role": "coLeader",
            "expLevel": 12,
            "trophies": 4855,
            "donations": 984,
            "donationsReceived": 600,
            "donationsDelta": 384,
            "arena": {
                "name": "Challenger III",
                "arena": "League 3",
                "arenaID": 15,
                "trophyLimit": 4600
            },
            "donationsPercent": 2.24
        },
        {
            "name": "Guilliton",
            "tag": "RVV82QP",
            "rank": 26,
            "previousRank": 21,
            "role": "coLeader",
            "expLevel": 13,
            "trophies": 4849,
            "donations": 190,
            "donationsReceived": 320,
            "donationsDelta": -130,
            "arena": {
                "name": "Challenger III",
                "arena": "League 3",
                "arenaID": 15,
                "trophyLimit": 4600
            },
            "donationsPercent": 0.43
        },
        {
            "name": "♠♥♦♣BooooMZ♣♦♥♠",
            "tag": "88L9GPPR",
            "rank": 27,
            "previousRank": 27,
            "role": "coLeader",
            "expLevel": 13,
            "trophies": 4824,
            "donations": 60,
            "donationsReceived": 200,
            "donationsDelta": -140,
            "arena": {
                "name": "Challenger III",
                "arena": "League 3",
                "arenaID": 15,
                "trophyLimit": 4600
            },
            "donationsPercent": 0.14
        },
        {
            "name": "SergileTTe",
            "tag": "2P89Q88G",
            "rank": 28,
            "previousRank": 29,
            "role": "elder",
            "expLevel": 12,
            "trophies": 4811,
            "donations": 308,
            "donationsReceived": 320,
            "donationsDelta": -12,
            "arena": {
                "name": "Challenger III",
                "arena": "League 3",
                "arenaID": 15,
                "trophyLimit": 4600
            },
            "donationsPercent": 0.7
        },
        {
            "name": "Ivan",
            "tag": "2290RRUC9",
            "rank": 29,
            "previousRank": 31,
            "role": "coLeader",
            "expLevel": 13,
            "trophies": 4806,
            "donations": 392,
            "donationsReceived": 520,
            "donationsDelta": -128,
            "arena": {
                "name": "Challenger III",
                "arena": "League 3",
                "arenaID": 15,
                "trophyLimit": 4600
            },
            "donationsPercent": 0.89
        },
        {
            "name": "kalico",
            "tag": "Q0JUQLRR",
            "rank": 30,
            "previousRank": 25,
            "role": "elder",
            "expLevel": 12,
            "trophies": 4804,
            "donations": 132,
            "donationsReceived": 440,
            "donationsDelta": -308,
            "arena": {
                "name": "Challenger III",
                "arena": "League 3",
                "arenaID": 15,
                "trophyLimit": 4600
            },
            "donationsPercent": 0.3
        },
        {
            "name": "Golem de agua",
            "tag": "LPC0GYY",
            "rank": 31,
            "previousRank": 37,
            "role": "elder",
            "expLevel": 12,
            "trophies": 4803,
            "donations": 257,
            "donationsReceived": 400,
            "donationsDelta": -143,
            "arena": {
                "name": "Challenger III",
                "arena": "League 3",
                "arenaID": 15,
                "trophyLimit": 4600
            },
            "donationsPercent": 0.58
        },
        {
            "name": "Adrigm97",
            "tag": "PVUVCPC2",
            "rank": 32,
            "previousRank": 30,
            "role": "elder",
            "expLevel": 12,
            "trophies": 4779,
            "donations": 316,
            "donationsReceived": 200,
            "donationsDelta": 116,
            "arena": {
                "name": "Challenger III",
                "arena": "League 3",
                "arenaID": 15,
                "trophyLimit": 4600
            },
            "donationsPercent": 0.72
        },
        {
            "name": "Des",
            "tag": "UJPG9Q2J",
            "rank": 33,
            "previousRank": 36,
            "role": "coLeader",
            "expLevel": 12,
            "trophies": 4778,
            "donations": 978,
            "donationsReceived": 440,
            "donationsDelta": 538,
            "arena": {
                "name": "Challenger III",
                "arena": "League 3",
                "arenaID": 15,
                "trophyLimit": 4600
            },
            "donationsPercent": 2.22
        },
        {
            "name": "Eme.connde",
            "tag": "2YYQYVVCJ",
            "rank": 34,
            "previousRank": 28,
            "role": "coLeader",
            "expLevel": 12,
            "trophies": 4769,
            "donations": 752,
            "donationsReceived": 440,
            "donationsDelta": 312,
            "arena": {
                "name": "Challenger III",
                "arena": "League 3",
                "arenaID": 15,
                "trophyLimit": 4600
            },
            "donationsPercent": 1.71
        },
        {
            "name": "SergioBola",
            "tag": "2YU900JPG",
            "rank": 35,
            "previousRank": 32,
            "role": "elder",
            "expLevel": 12,
            "trophies": 4751,
            "donations": 250,
            "donationsReceived": 230,
            "donationsDelta": 20,
            "arena": {
                "name": "Challenger III",
                "arena": "League 3",
                "arenaID": 15,
                "trophyLimit": 4600
            },
            "donationsPercent": 0.57
        },
        {
            "name": "Mikel",
            "tag": "8UVPJ8UR",
            "rank": 36,
            "previousRank": 38,
            "role": "coLeader",
            "expLevel": 12,
            "trophies": 4749,
            "donations": 438,
            "donationsReceived": 400,
            "donationsDelta": 38,
            "arena": {
                "name": "Challenger III",
                "arena": "League 3",
                "arenaID": 15,
                "trophyLimit": 4600
            },
            "donationsPercent": 1
        },
        {
            "name": "YourFkngGranpa",
            "tag": "2PU9202V",
            "rank": 37,
            "previousRank": 33,
            "role": "elder",
            "expLevel": 13,
            "trophies": 4741,
            "donations": 300,
            "donationsReceived": 430,
            "donationsDelta": -130,
            "arena": {
                "name": "Challenger III",
                "arena": "League 3",
                "arenaID": 15,
                "trophyLimit": 4600
            },
            "donationsPercent": 0.68
        },
        {
            "name": "Mosquito23",
            "tag": "9LCY9Y2J",
            "rank": 38,
            "previousRank": 35,
            "role": "elder",
            "expLevel": 13,
            "trophies": 4737,
            "donations": 676,
            "donationsReceived": 520,
            "donationsDelta": 156,
            "arena": {
                "name": "Challenger III",
                "arena": "League 3",
                "arenaID": 15,
                "trophyLimit": 4600
            },
            "donationsPercent": 1.54
        },
        {
            "name": "sodein14",
            "tag": "2CR82YPG",
            "rank": 39,
            "previousRank": 42,
            "role": "coLeader",
            "expLevel": 12,
            "trophies": 4731,
            "donations": 733,
            "donationsReceived": 560,
            "donationsDelta": 173,
            "arena": {
                "name": "Challenger III",
                "arena": "League 3",
                "arenaID": 15,
                "trophyLimit": 4600
            },
            "donationsPercent": 1.67
        },
        {
            "name": "Rayan",
            "tag": "PYQC8C2G",
            "rank": 40,
            "previousRank": 39,
            "role": "elder",
            "expLevel": 13,
            "trophies": 4707,
            "donations": 210,
            "donationsReceived": 400,
            "donationsDelta": -190,
            "arena": {
                "name": "Challenger III",
                "arena": "League 3",
                "arenaID": 15,
                "trophyLimit": 4600
            },
            "donationsPercent": 0.48
        },
        {
            "name": "jmpv",
            "tag": "2GQGUGQQ8",
            "rank": 41,
            "previousRank": 40,
            "role": "elder",
            "expLevel": 12,
            "trophies": 4690,
            "donations": 179,
            "donationsReceived": 360,
            "donationsDelta": -181,
            "arena": {
                "name": "Challenger III",
                "arena": "League 3",
                "arenaID": 15,
                "trophyLimit": 4600
            },
            "donationsPercent": 0.41
        },
        {
            "name": "Didac",
            "tag": "G0L9U9UQ",
            "rank": 42,
            "previousRank": 46,
            "role": "member",
            "expLevel": 12,
            "trophies": 4621,
            "donations": 86,
            "donationsReceived": 80,
            "donationsDelta": 6,
            "arena": {
                "name": "Challenger III",
                "arena": "League 3",
                "arenaID": 15,
                "trophyLimit": 4600
            },
            "donationsPercent": 0.2
        },
        {
            "name": "lillo",
            "tag": "8GJ9JC98Y",
            "rank": 43,
            "previousRank": 48,
            "role": "elder",
            "expLevel": 12,
            "trophies": 4618,
            "donations": 740,
            "donationsReceived": 540,
            "donationsDelta": 200,
            "arena": {
                "name": "Challenger III",
                "arena": "League 3",
                "arenaID": 15,
                "trophyLimit": 4600
            },
            "donationsPercent": 1.68
        },
        {
            "name": "magicsenna",
            "tag": "29VJGR808",
            "rank": 44,
            "previousRank": 41,
            "role": "elder",
            "expLevel": 12,
            "trophies": 4612,
            "donations": 220,
            "donationsReceived": 200,
            "donationsDelta": 20,
            "arena": {
                "name": "Challenger III",
                "arena": "League 3",
                "arenaID": 15,
                "trophyLimit": 4600
            },
            "donationsPercent": 0.5
        },
        {
            "name": "Joselu Fire⭐",
            "tag": "29GVURQUV",
            "rank": 45,
            "previousRank": 44,
            "role": "elder",
            "expLevel": 12,
            "trophies": 4612,
            "donations": 155,
            "donationsReceived": 320,
            "donationsDelta": -165,
            "arena": {
                "name": "Challenger III",
                "arena": "League 3",
                "arenaID": 15,
                "trophyLimit": 4600
            },
            "donationsPercent": 0.35
        },
        {
            "name": "Sanxsp",
            "tag": "8PQ9VVPG8",
            "rank": 46,
            "previousRank": 50,
            "role": "elder",
            "expLevel": 12,
            "trophies": 4555,
            "donations": 534,
            "donationsReceived": 520,
            "donationsDelta": 14,
            "arena": {
                "name": "Challenger II",
                "arena": "League 2",
                "arenaID": 14,
                "trophyLimit": 4300
            },
            "donationsPercent": 1.21
        },
        {
            "name": "fruticola",
            "tag": "8GVJYQ8G",
            "rank": 47,
            "previousRank": 47,
            "role": "coLeader",
            "expLevel": 13,
            "trophies": 4553,
            "donations": 748,
            "donationsReceived": 440,
            "donationsDelta": 308,
            "arena": {
                "name": "Challenger II",
                "arena": "League 2",
                "arenaID": 14,
                "trophyLimit": 4300
            },
            "donationsPercent": 1.7
        },
        {
            "name": "maitee",
            "tag": "28GCCQ2L",
            "rank": 48,
            "previousRank": 45,
            "role": "coLeader",
            "expLevel": 12,
            "trophies": 4534,
            "donations": 276,
            "donationsReceived": 360,
            "donationsDelta": -84,
            "arena": {
                "name": "Challenger II",
                "arena": "League 2",
                "arenaID": 14,
                "trophyLimit": 4300
            },
            "donationsPercent": 0.63
        },
        {
            "name": "ABEL",
            "tag": "PYRL2CLU",
            "rank": 49,
            "previousRank": 49,
            "role": "elder",
            "expLevel": 12,
            "trophies": 4527,
            "donations": 140,
            "donationsReceived": 240,
            "donationsDelta": -100,
            "arena": {
                "name": "Challenger II",
                "arena": "League 2",
                "arenaID": 14,
                "trophyLimit": 4300
            },
            "donationsPercent": 0.32
        },
        {
            "name": "yisu",
            "tag": "9UUJRJYR",
            "rank": 50,
            "previousRank": 43,
            "role": "elder",
            "expLevel": 12,
            "trophies": 4476,
            "donations": 244,
            "donationsReceived": 280,
            "donationsDelta": -36,
            "arena": {
                "name": "Challenger II",
                "arena": "League 2",
                "arenaID": 14,
                "trophyLimit": 4300
            },
            "donationsPercent": 0.55
        }
    ],
    "tracking": {
        "active": false,
        "available": false,
        "snapshotCount": 0
    }
}