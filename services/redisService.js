const redis = require('redis');
const { promisify } = require('util');

let client
if (process.env.NODE_ENV === 'production')
    client = redis.createClient(process.env.REDIS_URL) // create and connect client to heroku redis instance. Just adding 'Heroku Redis' addon creates this variable automatically
else
    client = redis.createClient() // create and connect client to local redis instance.

const ttl = promisify(client.ttl).bind(client);
const getRemainingTime = async (key) => await ttl(key)


module.exports = {
  getRemainingTime, 
  client
}

