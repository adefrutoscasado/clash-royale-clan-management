const axios = require('axios');
const moment = require('moment');
const humanizeDuration = require('humanize-duration');
const CONSTANTS = require('../constants/constants');
const {EXPIRE_TIME} = require('../constants/redis');
const sampleResponse = require('../debug/IndexResponse');
const redis = require('redis');
const {client, getRemainingTime} = require('./../services/redisService')
const CLASH_ROYALE_TOKEN = CONSTANTS.CLASH_ROYALE_TOKEN
const REFRESH_NUM_RETRIES = 3

let debug
(process.env.NODE_ENV === 'production') ? debug = false : debug = true

// const client = redis.createClient();
const {promisify} = require('util');
const getAsync = promisify(client.get).bind(client);

// Print redis errors to the console
client.on('error', (err) => {
    console.log("Error " + err);
});

const http = axios.create({
    headers: { 'Cache-Control': 'no-cache' },
    timeout: 60000,
    // cache will be enabled by default
    // adapter: cacheAdapterEnhancer(axios.defaults.adapter)
}
)

let config = {
    headers: {
        Authorization: `Bearer ${CLASH_ROYALE_TOKEN}`,
    }
}

const clashRoyaleBaseUrl = 'https://api.royaleapi.com'

getWarlogUrl = (clanTag) => `${clashRoyaleBaseUrl}/clan/${clanTag}/warlog`
getClanUrl = (clanTag) => `${clashRoyaleBaseUrl}/clan/${clanTag}`
getPlayerUrl = (playerTag) => `${clashRoyaleBaseUrl}/player/${playerTag}`

let getFromRedis = async (url) => {
    const response = await getAsync(url) // returns null if not found
    if (!response) throw new Error
    // console.log(moment())
    let responseJSON = JSON.parse(response)
    return responseJSON
}

let getFromApi = async (url) => {
    const response = await http.get(url, config)
    const data = response.data
    // Save response in Redis store
    client.setex(url, EXPIRE_TIME, JSON.stringify(data)); // seconds to expire
    // console.log(moment())
    return data
}

let getData = async (url, forceUpdate) => {
    console.log('getData:' + url)
    if (forceUpdate) {
        let i;
        for (i = 0; i < REFRESH_NUM_RETRIES; ++i) {
            try {
                let data = await getFromApi(url)
                return data
            } catch(err) {
                if (i < REFRESH_NUM_RETRIES) console.error('Api request failed. Retrying')
                else console.error('Api request failed. Abort')
                console.error('Errors status ' + err.status)
                console.error(err.message)
            }
        }
    } else {
        try {
            return await getFromRedis(url)
        } catch (err) {
            try {
                return await getFromApi(url)
            } catch (err) {
                console.error('API request failed')
                console.error('Errors status ' + err.status)
                console.error(err.message)
            }
        }
    }
}

let getLastUpdateTime = async (clanTag) => {
    let remainingTime = (await getRemainingTime(getWarlogUrl(clanTag)))*1000 // milisecs
    let timeSinceLastUpdate = EXPIRE_TIME*1000 - remainingTime
    return humanizeDuration(timeSinceLastUpdate, { largest: 2, language: 'es' }) 
}

module.exports={
    getWarlog: async (clanTag, forceUpdate = false) => {
        if (debug) return sampleResponse.warlogResponse
        let url = getWarlogUrl(clanTag)
        return await getData(url, forceUpdate)
    },
    getClan: async (clanTag, forceUpdate = false) => {
        if (debug) return sampleResponse.clanResponse
        url = getClanUrl(clanTag)
        return await getData(url, forceUpdate)
    },
    getPlayer: async (playerTag, forceUpdate = false) => {
        if (debug) return sampleResponse.playerResponse
        let url = getPlayerUrl(playerTag)
        return await getData(url, forceUpdate)
    },
    getLastUpdateTime
}
