var express = require('express');
var indexController = require('../controllers/indexController');
var refreshController = require('../controllers/refreshController');
var router = express.Router();

router.route('/').get(
  indexController.sendIndex
)

router.route('/invernalia/:clanDivision').get(
  indexController.sendIndex
)

router.route('/invernalia/:clanDivision/refresh').get(
  refreshController.refreshClanData
)

module.exports = router;
