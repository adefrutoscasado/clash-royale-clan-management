'use strict'

const arrayService = require('../services/arrayService');
const moment = require('moment')
const momentDurationFormatSetup = require("moment-duration-format");
const CONSTANTS = require('../constants/constants');


const self = {

    getArrayLevelsByRarity: (playerResponse, rarity) => {
        let cards = playerResponse.cards
        let cardsFiltered = cards.filter(card => {
            return card.rarity === rarity
        })
        let cardsLevels = cardsFiltered.map(card => {
            return card.level
        })
        return cardsLevels
    },
    getMeanLevelByRarity: (playerResponse, rarity) => {
        return arrayService.mean(self.getArrayLevelsByRarity(playerResponse, rarity))
    },
    getVarianceLevelByRarity: (playerResponse, rarity) => {
        return arrayService.variance(self.getArrayLevelsByRarity(playerResponse, rarity))
    },
    calculateCardCost: () => {
        
    },
    getCardGoldSpent: (cardLevel, rarity) => {
        let cost = []
        if (rarity === CONSTANTS.RARITY.COMMON_NAME)
            cost = CONSTANTS.COST.COMMON_COST
        if (rarity === CONSTANTS.RARITY.RARE_NAME)
            cost = CONSTANTS.COST.RARE_COST
        if (rarity === CONSTANTS.RARITY.EPIC_NAME)
            cost = CONSTANTS.COST.EPIC_COST
        if (rarity === CONSTANTS.RARITY.LEGENDARY_NAME)
            cost = CONSTANTS.COST.LEGENDARY_COST


        let total = 0
        for(var i=0; i <= cardLevel; i++){
            total = total + cost[i];   
        }
        return total
    },
    getCardsGoldSpent: (playerResponse) => {
        let commonArrayLevels = self.getArrayLevelsByRarity(playerResponse, CONSTANTS.RARITY.COMMON_NAME)
        let commonArrayCost = commonArrayLevels.map((cardLevel)=>{
            return self.getCardGoldSpent(cardLevel, CONSTANTS.RARITY.COMMON_NAME)
        })
        let rareArrayLevels = self.getArrayLevelsByRarity(playerResponse, CONSTANTS.RARITY.RARE_NAME)
        let rareArrayCost = rareArrayLevels.map((cardLevel)=>{
            return self.getCardGoldSpent(cardLevel, CONSTANTS.RARITY.RARE_NAME)
        })
        let epicArrayLevels = self.getArrayLevelsByRarity(playerResponse, CONSTANTS.RARITY.EPIC_NAME)
        let epicArrayCost = epicArrayLevels.map((cardLevel)=>{
            return self.getCardGoldSpent(cardLevel, CONSTANTS.RARITY.EPIC_NAME)
        })
        let legendaryArrayLevels = self.getArrayLevelsByRarity(playerResponse, CONSTANTS.RARITY.LEGENDARY_NAME)
        let legendaryArrayCost = legendaryArrayLevels.map((cardLevel)=>{
            return self.getCardGoldSpent(cardLevel, CONSTANTS.RARITY.LEGENDARY_NAME)
        })

        let commonCost = commonArrayCost.reduce((v, i) => (v + i));
        let rareCost = rareArrayCost.reduce((v, i) => (v + i));
        let epicCost = epicArrayCost.reduce((v, i) => (v + i));
        let legendaryCost = legendaryArrayCost.reduce((v, i) => (v + i));
        return commonCost+rareCost+epicCost+legendaryCost
    },
    getDeckLink: (playerResponse) => {
        return playerResponse.deckLink
    },
    getCurrentDeck: (playerResponse) => {
        return playerResponse.currentDeck
    },
    getTotalGames: (playerResponse) => {
        return playerResponse.games.total
    },
    getWinsGames: (playerResponse) => {
        return playerResponse.games.wins
    },
    getLossesGames: (playerResponse) => {
        return playerResponse.games.losses
    },
    getDrawsGames: (playerResponse) => {
        return playerResponse.games.draws
    },
    getWinsPercentGames: (playerResponse) => {
        return playerResponse.games.winsPercent
    },
    getLossesPercentGames: (playerResponse) => {
        return playerResponse.games.lossesPercent
    },
    getDrawsPercentGames: (playerResponse) => {
        return playerResponse.games.drawsPercent
    },
    getDrawsPercentGames: (playerResponse) => {
        return playerResponse.games.drawsPercent
    },
    getThreeCrownWinsStats: (playerResponse) => {
        return playerResponse.stats.threeCrownWins
    },
    getTimeSpentPlaying: (playerResponse) => {
        return moment.duration((Number(playerResponse.games.total)*3), "minutes").format("dd:hh:mm")
    },
    getThreeCrownWinsRatio: (playerResponse) => {
        return Number(playerResponse.stats.threeCrownWins)/Number(playerResponse.games.total)
    },
}

module.exports = self