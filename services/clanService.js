'use strict'

const arrayService = require('../services/arrayService');

const self = {
    getMembers: (clanResponse) => {
        let members = []
        clanResponse.members.forEach(member => {
            members.push(member.tag)
        })
        return members
    },
    getNameByTag: (clanResponse, PlayerTag) => {
        let player = clanResponse.members.find(element => {
            return element.tag === PlayerTag;
        });
        let name = player ? player.name : 'Player not found'
        return name
    },
}

module.exports = self