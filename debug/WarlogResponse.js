module.exports = 
[
    {
        "createdDate": 1535722117,
        "participants": [
            {
                "tag": "CQ82YRR8",
                "name": "Adrián M",
                "cardsEarned": 2970,
                "battlesPlayed": 1,
                "wins": 1
            },
            {
                "tag": "GPCR09JL",
                "name": "★★★COROCOTA★★★",
                "cardsEarned": 2805,
                "battlesPlayed": 1,
                "wins": 1
            },
            {
                "tag": "2J8PGCPUC",
                "name": "nawan",
                "cardsEarned": 2337,
                "battlesPlayed": 1,
                "wins": 1
            },
            {
                "tag": "2VC9GULRV",
                "name": "joker",
                "cardsEarned": 2337,
                "battlesPlayed": 1,
                "wins": 1
            },
            {
                "tag": "8P9LV88Y",
                "name": "guyur",
                "cardsEarned": 2337,
                "battlesPlayed": 1,
                "wins": 1
            },
            {
                "tag": "2QGUUJV9",
                "name": "Maestre Sam",
                "cardsEarned": 2337,
                "battlesPlayed": 1,
                "wins": 1
            },
            {
                "tag": "RVV82QP",
                "name": "Guilliton",
                "cardsEarned": 2337,
                "battlesPlayed": 1,
                "wins": 1
            },
            {
                "tag": "90GP0RQ9",
                "name": "vulosky",
                "cardsEarned": 2337,
                "battlesPlayed": 1,
                "wins": 1
            },
            {
                "tag": "9CCLRG9Y",
                "name": "lebowsky",
                "cardsEarned": 2337,
                "battlesPlayed": 1,
                "wins": 1
            },
            {
                "tag": "2290RRUC9",
                "name": "Ivan",
                "cardsEarned": 2200,
                "battlesPlayed": 1,
                "wins": 1
            },
            {
                "tag": "UJPG9Q2J",
                "name": "Des",
                "cardsEarned": 2200,
                "battlesPlayed": 1,
                "wins": 1
            },
            {
                "tag": "PVUVCPC2",
                "name": "Adrigm97",
                "cardsEarned": 2200,
                "battlesPlayed": 1,
                "wins": 1
            },
            {
                "tag": "Y820L8LP",
                "name": "PuxiWeed",
                "cardsEarned": 1869,
                "battlesPlayed": 1,
                "wins": 1
            },
            {
                "tag": "YLL2YG0Q",
                "name": "SeRmAo",
                "cardsEarned": 1869,
                "battlesPlayed": 1,
                "wins": 1
            },
            {
                "tag": "8G8GCQRJ",
                "name": "Mikky",
                "cardsEarned": 1869,
                "battlesPlayed": 1,
                "wins": 1
            },
            {
                "tag": "88LQUVPG",
                "name": "Asten",
                "cardsEarned": 1869,
                "battlesPlayed": 1,
                "wins": 1
            },
            {
                "tag": "JJP9JUG2",
                "name": "#Núñez",
                "cardsEarned": 1869,
                "battlesPlayed": 1,
                "wins": 1
            },
            {
                "tag": "2CR82YPG",
                "name": "sodein14",
                "cardsEarned": 1760,
                "battlesPlayed": 1,
                "wins": 1
            },
            {
                "tag": "2PU9202V",
                "name": "YourFkngGranpa",
                "cardsEarned": 1760,
                "battlesPlayed": 1,
                "wins": 1
            },
            {
                "tag": "2GQGUGQQ8",
                "name": "jmpv",
                "cardsEarned": 1760,
                "battlesPlayed": 1,
                "wins": 1
            },
            {
                "tag": "22Q2RCJVV",
                "name": "GaliciaNoMapa",
                "cardsEarned": 1401,
                "battlesPlayed": 1,
                "wins": 1
            },
            {
                "tag": "LPC0GYY",
                "name": "Golem de agua",
                "cardsEarned": 1320,
                "battlesPlayed": 1,
                "wins": 1
            },
            {
                "tag": "PYQC8C2G",
                "name": "Rayan",
                "cardsEarned": 1320,
                "battlesPlayed": 1,
                "wins": 1
            },
            {
                "tag": "PYRL2CLU",
                "name": "ABEL",
                "cardsEarned": 1320,
                "battlesPlayed": 1,
                "wins": 1
            },
            {
                "tag": "22P0YP82",
                "name": "AustinSantoS",
                "cardsEarned": 2805,
                "battlesPlayed": 1,
                "wins": 0
            },
            {
                "tag": "PYL8PPJ8",
                "name": "Ragnar",
                "cardsEarned": 2337,
                "battlesPlayed": 1,
                "wins": 0
            },
            {
                "tag": "U2YCLPQU",
                "name": "er tabu",
                "cardsEarned": 2337,
                "battlesPlayed": 1,
                "wins": 0
            },
            {
                "tag": "2PR0999J",
                "name": "SEIMUS",
                "cardsEarned": 2337,
                "battlesPlayed": 1,
                "wins": 0
            },
            {
                "tag": "8GVJYQ8G",
                "name": "fruticola",
                "cardsEarned": 2200,
                "battlesPlayed": 1,
                "wins": 0
            },
            {
                "tag": "8GJ9JC98Y",
                "name": "lillo",
                "cardsEarned": 2200,
                "battlesPlayed": 1,
                "wins": 0
            },
            {
                "tag": "2P89Q88G",
                "name": "SergileTTe",
                "cardsEarned": 2200,
                "battlesPlayed": 1,
                "wins": 0
            },
            {
                "tag": "88L9GPPR",
                "name": "♠♥♦♣BooooMZ♣♦♥♠",
                "cardsEarned": 2200,
                "battlesPlayed": 1,
                "wins": 0
            },
            {
                "tag": "8PQ9VVPG8",
                "name": "Sanxsp",
                "cardsEarned": 2200,
                "battlesPlayed": 1,
                "wins": 0
            },
            {
                "tag": "8UVPJ8UR",
                "name": "Mikel",
                "cardsEarned": 2200,
                "battlesPlayed": 1,
                "wins": 0
            },
            {
                "tag": "28GCCQ2L",
                "name": "maitee",
                "cardsEarned": 2200,
                "battlesPlayed": 1,
                "wins": 0
            },
            {
                "tag": "G9J9CPL",
                "name": "S.J.P",
                "cardsEarned": 1869,
                "battlesPlayed": 1,
                "wins": 0
            },
            {
                "tag": "QPCJQJQ",
                "name": "Jimlou",
                "cardsEarned": 1869,
                "battlesPlayed": 1,
                "wins": 0
            },
            {
                "tag": "L20PVCVC",
                "name": "Luke Boss",
                "cardsEarned": 1869,
                "battlesPlayed": 1,
                "wins": 0
            },
            {
                "tag": "2QVQUCP",
                "name": "Maty97ro",
                "cardsEarned": 1869,
                "battlesPlayed": 1,
                "wins": 0
            },
            {
                "tag": "9UUJRJYR",
                "name": "yisu",
                "cardsEarned": 1760,
                "battlesPlayed": 1,
                "wins": 0
            },
            {
                "tag": "Q0JUQLRR",
                "name": "kalico",
                "cardsEarned": 1760,
                "battlesPlayed": 1,
                "wins": 0
            },
            {
                "tag": "U08RLC2U",
                "name": "Nayper",
                "cardsEarned": 1760,
                "battlesPlayed": 1,
                "wins": 0
            },
            {
                "tag": "2YYQYVVCJ",
                "name": "Eme.connde",
                "cardsEarned": 1760,
                "battlesPlayed": 1,
                "wins": 0
            },
            {
                "tag": "2LLYC9PPC",
                "name": "sifh21",
                "cardsEarned": 1760,
                "battlesPlayed": 1,
                "wins": 0
            },
            {
                "tag": "29GVURQUV",
                "name": "Joselu Fire⭐",
                "cardsEarned": 1320,
                "battlesPlayed": 1,
                "wins": 0
            },
            {
                "tag": "9LCY9Y2J",
                "name": "Mosquito23",
                "cardsEarned": 1320,
                "battlesPlayed": 1,
                "wins": 0
            },
            {
                "tag": "2G9YCGVG",
                "name": "Pretoriano",
                "cardsEarned": 1760,
                "battlesPlayed": 0,
                "wins": 0
            }
        ],
        "standings": [
            {
                "tag": "8JPCJU0R",
                "name": "我愛瑪莉小羊 - 零",
                "participants": 46,
                "battlesPlayed": 47,
                "wins": 36,
                "crowns": 59,
                "warTrophies": 4091,
                "warTrophiesChange": 136,
                "badge": {
                    "name": "Lotus_01",
                    "category": "01_Symbol",
                    "id": 16000142,
                    "image": "https://royaleapi.github.io/cr-api-assets/badges/Lotus_01.png"
                }
            },
            {
                "tag": "92CY2898",
                "name": "Kuwait",
                "participants": 44,
                "battlesPlayed": 47,
                "wins": 25,
                "crowns": 46,
                "warTrophies": 3295,
                "warTrophiesChange": 75,
                "badge": {
                    "name": "Moon_01",
                    "category": "01_Symbol",
                    "id": 16000030,
                    "image": "https://royaleapi.github.io/cr-api-assets/badges/Moon_01.png"
                }
            },
            {
                "tag": "PPYPCV",
                "name": "INVERNALIA",
                "participants": 47,
                "battlesPlayed": 46,
                "wins": 24,
                "crowns": 37,
                "warTrophies": 3008,
                "warTrophiesChange": -1,
                "badge": {
                    "name": "Freeze_01",
                    "category": "01_Symbol",
                    "id": 16000168,
                    "image": "https://royaleapi.github.io/cr-api-assets/badges/Freeze_01.png"
                }
            },
            {
                "tag": "9LRPLCPQ",
                "name": "BosnianLegends",
                "participants": 43,
                "battlesPlayed": 46,
                "wins": 22,
                "crowns": 36,
                "warTrophies": 3228,
                "warTrophiesChange": -28,
                "badge": {
                    "name": "flag_j_03",
                    "category": "02_Flag",
                    "id": 16000077,
                    "image": "https://royaleapi.github.io/cr-api-assets/badges/flag_j_03.png"
                }
            },
            {
                "tag": "89QYYU98",
                "name": "! ⏩⚡ ISSOU ⚡⏪ !",
                "participants": 41,
                "battlesPlayed": 46,
                "wins": 10,
                "crowns": 16,
                "warTrophies": 3011,
                "warTrophiesChange": -90,
                "badge": {
                    "name": "flag_k_01",
                    "category": "02_Flag",
                    "id": 16000078,
                    "image": "https://royaleapi.github.io/cr-api-assets/badges/flag_k_01.png"
                }
            }
        ],
        "seasonNumber": 10
    },
    {
        "createdDate": 1535549290,
        "participants": [
            {
                "tag": "JJP9JUG2",
                "name": "#Núñez",
                "cardsEarned": 1120,
                "battlesPlayed": 2,
                "wins": 2
            },
            {
                "tag": "8UVPJ8UR",
                "name": "Mikel",
                "cardsEarned": 1120,
                "battlesPlayed": 2,
                "wins": 2
            },
            {
                "tag": "PVUVCPC2",
                "name": "Adrigm97",
                "cardsEarned": 1400,
                "battlesPlayed": 2,
                "wins": 1
            },
            {
                "tag": "29GVURQUV",
                "name": "Joselu Fire⭐",
                "cardsEarned": 1120,
                "battlesPlayed": 2,
                "wins": 1
            },
            {
                "tag": "RVV82QP",
                "name": "Guilliton",
                "cardsEarned": 1785,
                "battlesPlayed": 1,
                "wins": 1
            },
            {
                "tag": "2VC9GULRV",
                "name": "joker",
                "cardsEarned": 1750,
                "battlesPlayed": 1,
                "wins": 1
            },
            {
                "tag": "2290RRUC9",
                "name": "Ivan",
                "cardsEarned": 1680,
                "battlesPlayed": 1,
                "wins": 1
            },
            {
                "tag": "2CR82YPG",
                "name": "sodein14",
                "cardsEarned": 1680,
                "battlesPlayed": 1,
                "wins": 1
            },
            {
                "tag": "2J8PGCPUC",
                "name": "nawan",
                "cardsEarned": 1487,
                "battlesPlayed": 1,
                "wins": 1
            },
            {
                "tag": "L20PVCVC",
                "name": "Luke Boss",
                "cardsEarned": 1487,
                "battlesPlayed": 1,
                "wins": 1
            },
            {
                "tag": "2PR0999J",
                "name": "SEIMUS",
                "cardsEarned": 1487,
                "battlesPlayed": 1,
                "wins": 1
            },
            {
                "tag": "PYQC8C2G",
                "name": "Rayan",
                "cardsEarned": 1400,
                "battlesPlayed": 1,
                "wins": 1
            },
            {
                "tag": "9CCLRG9Y",
                "name": "lebowsky",
                "cardsEarned": 1400,
                "battlesPlayed": 1,
                "wins": 1
            },
            {
                "tag": "2GQGUGQQ8",
                "name": "jmpv",
                "cardsEarned": 1400,
                "battlesPlayed": 1,
                "wins": 1
            },
            {
                "tag": "CQ82YRR8",
                "name": "Adrián M",
                "cardsEarned": 1260,
                "battlesPlayed": 1,
                "wins": 1
            },
            {
                "tag": "Y820L8LP",
                "name": "PuxiWeed",
                "cardsEarned": 1189,
                "battlesPlayed": 1,
                "wins": 1
            },
            {
                "tag": "YLL2YG0Q",
                "name": "SeRmAo",
                "cardsEarned": 1189,
                "battlesPlayed": 1,
                "wins": 1
            },
            {
                "tag": "U2YCLPQU",
                "name": "er tabu",
                "cardsEarned": 1189,
                "battlesPlayed": 1,
                "wins": 1
            },
            {
                "tag": "88LQUVPG",
                "name": "Asten",
                "cardsEarned": 1189,
                "battlesPlayed": 1,
                "wins": 1
            },
            {
                "tag": "8P9LV88Y",
                "name": "guyur",
                "cardsEarned": 1189,
                "battlesPlayed": 1,
                "wins": 1
            },
            {
                "tag": "QPCJQJQ",
                "name": "Jimlou",
                "cardsEarned": 1120,
                "battlesPlayed": 1,
                "wins": 1
            },
            {
                "tag": "8GVJYQ8G",
                "name": "fruticola",
                "cardsEarned": 1120,
                "battlesPlayed": 1,
                "wins": 1
            },
            {
                "tag": "UJPG9Q2J",
                "name": "Des",
                "cardsEarned": 1120,
                "battlesPlayed": 1,
                "wins": 1
            },
            {
                "tag": "Q0JUQLRR",
                "name": "kalico",
                "cardsEarned": 1120,
                "battlesPlayed": 1,
                "wins": 1
            },
            {
                "tag": "G9J9CPL",
                "name": "S.J.P",
                "cardsEarned": 891,
                "battlesPlayed": 1,
                "wins": 1
            },
            {
                "tag": "GPCR09JL",
                "name": "★★★COROCOTA★★★",
                "cardsEarned": 891,
                "battlesPlayed": 1,
                "wins": 1
            },
            {
                "tag": "22Q2RCJVV",
                "name": "GaliciaNoMapa",
                "cardsEarned": 891,
                "battlesPlayed": 1,
                "wins": 1
            },
            {
                "tag": "LPC0GYY",
                "name": "Golem de agua",
                "cardsEarned": 840,
                "battlesPlayed": 1,
                "wins": 1
            },
            {
                "tag": "2PU9202V",
                "name": "YourFkngGranpa",
                "cardsEarned": 840,
                "battlesPlayed": 1,
                "wins": 1
            },
            {
                "tag": "28GCCQ2L",
                "name": "maitee",
                "cardsEarned": 840,
                "battlesPlayed": 1,
                "wins": 1
            },
            {
                "tag": "U08RLC2U",
                "name": "Nayper",
                "cardsEarned": 1785,
                "battlesPlayed": 1,
                "wins": 0
            },
            {
                "tag": "2YYQYVVCJ",
                "name": "Eme.connde",
                "cardsEarned": 1680,
                "battlesPlayed": 1,
                "wins": 0
            },
            {
                "tag": "2LLYC9PPC",
                "name": "sifh21",
                "cardsEarned": 1680,
                "battlesPlayed": 1,
                "wins": 0
            },
            {
                "tag": "2QGUUJV9",
                "name": "Maestre Sam",
                "cardsEarned": 1487,
                "battlesPlayed": 1,
                "wins": 0
            },
            {
                "tag": "YCCQ8Q2Y",
                "name": "yonas",
                "cardsEarned": 1400,
                "battlesPlayed": 1,
                "wins": 0
            },
            {
                "tag": "2Y22P9V28",
                "name": "⚽cristianO_vk⚽",
                "cardsEarned": 1400,
                "battlesPlayed": 1,
                "wins": 0
            },
            {
                "tag": "2P89Q88G",
                "name": "SergileTTe",
                "cardsEarned": 1400,
                "battlesPlayed": 1,
                "wins": 0
            },
            {
                "tag": "JCCL9C2",
                "name": "LUDOPATEKU$$$$",
                "cardsEarned": 1400,
                "battlesPlayed": 1,
                "wins": 0
            },
            {
                "tag": "8G8GCQRJ",
                "name": "Mikky",
                "cardsEarned": 1189,
                "battlesPlayed": 1,
                "wins": 0
            },
            {
                "tag": "2QVQUCP",
                "name": "Maty97ro",
                "cardsEarned": 1189,
                "battlesPlayed": 1,
                "wins": 0
            },
            {
                "tag": "90GP0RQ9",
                "name": "vulosky",
                "cardsEarned": 1189,
                "battlesPlayed": 1,
                "wins": 0
            },
            {
                "tag": "8GJ9JC98Y",
                "name": "lillo",
                "cardsEarned": 1120,
                "battlesPlayed": 1,
                "wins": 0
            },
            {
                "tag": "9LCY9Y2J",
                "name": "Mosquito23",
                "cardsEarned": 1120,
                "battlesPlayed": 1,
                "wins": 0
            },
            {
                "tag": "88L9GPPR",
                "name": "♠♥♦♣BooooMZ♣♦♥♠",
                "cardsEarned": 1120,
                "battlesPlayed": 1,
                "wins": 0
            },
            {
                "tag": "8PQ9VVPG8",
                "name": "Sanxsp",
                "cardsEarned": 1120,
                "battlesPlayed": 1,
                "wins": 0
            },
            {
                "tag": "PYRL2CLU",
                "name": "ABEL",
                "cardsEarned": 840,
                "battlesPlayed": 1,
                "wins": 0
            }
        ],
        "standings": [
            {
                "tag": "282CQV8",
                "name": "JAPAN KNIGHT 7",
                "participants": 46,
                "battlesPlayed": 50,
                "wins": 37,
                "crowns": 52,
                "warTrophies": 3130,
                "warTrophiesChange": 137,
                "badge": {
                    "name": "Sword_03",
                    "category": "01_Symbol",
                    "id": 16000006,
                    "image": "https://royaleapi.github.io/cr-api-assets/badges/Sword_03.png"
                }
            },
            {
                "tag": "PPYPCV",
                "name": "INVERNALIA",
                "participants": 46,
                "battlesPlayed": 50,
                "wins": 32,
                "crowns": 53,
                "warTrophies": 3009,
                "warTrophiesChange": 82,
                "badge": {
                    "name": "Freeze_01",
                    "category": "01_Symbol",
                    "id": 16000168,
                    "image": "https://royaleapi.github.io/cr-api-assets/badges/Freeze_01.png"
                }
            },
            {
                "tag": "U9PGYU",
                "name": "까리용",
                "participants": 48,
                "battlesPlayed": 50,
                "wins": 30,
                "crowns": 42,
                "warTrophies": 2905,
                "warTrophiesChange": 5,
                "badge": {
                    "name": "Gem_04",
                    "category": "03_Royale",
                    "id": 16000101,
                    "image": "https://royaleapi.github.io/cr-api-assets/badges/Gem_04.png"
                }
            },
            {
                "tag": "82RQQC",
                "name": "광개토대왕",
                "participants": 50,
                "battlesPlayed": 47,
                "wins": 24,
                "crowns": 41,
                "warTrophies": 2554,
                "warTrophiesChange": -26,
                "badge": {
                    "name": "A_Char_Hammer_02",
                    "category": "03_Royale",
                    "id": 16000165,
                    "image": "https://royaleapi.github.io/cr-api-assets/badges/A_Char_Hammer_02.png"
                }
            },
            {
                "tag": "2LLPJUU",
                "name": "only　my　road",
                "participants": 41,
                "battlesPlayed": 49,
                "wins": 18,
                "crowns": 34,
                "warTrophies": 2419,
                "warTrophiesChange": -82,
                "badge": {
                    "name": "Cherry_Blossom_07",
                    "category": "01_Symbol",
                    "id": 16000134,
                    "image": "https://royaleapi.github.io/cr-api-assets/badges/Cherry_Blossom_07.png"
                }
            }
        ],
        "seasonNumber": 10
    },
    {
        "createdDate": 1535376464,
        "participants": [
            {
                "tag": "8P9LV88Y",
                "name": "guyur",
                "cardsEarned": 1785,
                "battlesPlayed": 1,
                "wins": 1
            },
            {
                "tag": "2VC9GULRV",
                "name": "joker",
                "cardsEarned": 1680,
                "battlesPlayed": 1,
                "wins": 1
            },
            {
                "tag": "2QVQUCP",
                "name": "Maty97ro",
                "cardsEarned": 1680,
                "battlesPlayed": 1,
                "wins": 1
            },
            {
                "tag": "88LQUVPG",
                "name": "Asten",
                "cardsEarned": 1680,
                "battlesPlayed": 1,
                "wins": 1
            },
            {
                "tag": "CQ82YRR8",
                "name": "Adrián M",
                "cardsEarned": 1522,
                "battlesPlayed": 1,
                "wins": 1
            },
            {
                "tag": "PYL8PPJ8",
                "name": "Ragnar",
                "cardsEarned": 1487,
                "battlesPlayed": 1,
                "wins": 1
            },
            {
                "tag": "Q0JUQLRR",
                "name": "kalico",
                "cardsEarned": 1487,
                "battlesPlayed": 1,
                "wins": 1
            },
            {
                "tag": "YR8LLUU8",
                "name": "ELVIS",
                "cardsEarned": 1487,
                "battlesPlayed": 1,
                "wins": 1
            },
            {
                "tag": "RVV82QP",
                "name": "Guilliton",
                "cardsEarned": 1487,
                "battlesPlayed": 1,
                "wins": 1
            },
            {
                "tag": "90GP0RQ9",
                "name": "vulosky",
                "cardsEarned": 1487,
                "battlesPlayed": 1,
                "wins": 1
            },
            {
                "tag": "8GVJYQ8G",
                "name": "fruticola",
                "cardsEarned": 1400,
                "battlesPlayed": 1,
                "wins": 1
            },
            {
                "tag": "QU0Y0Y2",
                "name": "Juan",
                "cardsEarned": 1400,
                "battlesPlayed": 1,
                "wins": 1
            },
            {
                "tag": "G9J9CPL",
                "name": "S.J.P",
                "cardsEarned": 1189,
                "battlesPlayed": 1,
                "wins": 1
            },
            {
                "tag": "JJP9JUG2",
                "name": "#Núñez",
                "cardsEarned": 1189,
                "battlesPlayed": 1,
                "wins": 1
            },
            {
                "tag": "8GJ9JC98Y",
                "name": "lillo",
                "cardsEarned": 1120,
                "battlesPlayed": 1,
                "wins": 1
            },
            {
                "tag": "QPCJQJQ",
                "name": "Jimlou",
                "cardsEarned": 1120,
                "battlesPlayed": 1,
                "wins": 1
            },
            {
                "tag": "9LCY9Y2J",
                "name": "Mosquito23",
                "cardsEarned": 1120,
                "battlesPlayed": 1,
                "wins": 1
            },
            {
                "tag": "9CCLRG9Y",
                "name": "lebowsky",
                "cardsEarned": 1120,
                "battlesPlayed": 1,
                "wins": 1
            },
            {
                "tag": "L20PVCVC",
                "name": "Luke Boss",
                "cardsEarned": 891,
                "battlesPlayed": 1,
                "wins": 1
            },
            {
                "tag": "U08RLC2U",
                "name": "Nayper",
                "cardsEarned": 840,
                "battlesPlayed": 1,
                "wins": 1
            },
            {
                "tag": "2GQGUGQQ8",
                "name": "jmpv",
                "cardsEarned": 840,
                "battlesPlayed": 1,
                "wins": 1
            },
            {
                "tag": "8G8GCQRJ",
                "name": "Mikky",
                "cardsEarned": 1785,
                "battlesPlayed": 1,
                "wins": 0
            },
            {
                "tag": "Y820L8LP",
                "name": "PuxiWeed",
                "cardsEarned": 1680,
                "battlesPlayed": 1,
                "wins": 0
            },
            {
                "tag": "UJPG9Q2J",
                "name": "Des",
                "cardsEarned": 1680,
                "battlesPlayed": 1,
                "wins": 0
            },
            {
                "tag": "2YYQYVVCJ",
                "name": "Eme.connde",
                "cardsEarned": 1680,
                "battlesPlayed": 1,
                "wins": 0
            },
            {
                "tag": "8UVPJ8UR",
                "name": "Mikel",
                "cardsEarned": 1680,
                "battlesPlayed": 1,
                "wins": 0
            },
            {
                "tag": "YLL2YG0Q",
                "name": "SeRmAo",
                "cardsEarned": 1487,
                "battlesPlayed": 1,
                "wins": 0
            },
            {
                "tag": "2J8PGCPUC",
                "name": "nawan",
                "cardsEarned": 1487,
                "battlesPlayed": 1,
                "wins": 0
            },
            {
                "tag": "2290RRUC9",
                "name": "Ivan",
                "cardsEarned": 1400,
                "battlesPlayed": 1,
                "wins": 0
            },
            {
                "tag": "2P89Q88G",
                "name": "SergileTTe",
                "cardsEarned": 1400,
                "battlesPlayed": 1,
                "wins": 0
            },
            {
                "tag": "88L9GPPR",
                "name": "♠♥♦♣BooooMZ♣♦♥♠",
                "cardsEarned": 1400,
                "battlesPlayed": 1,
                "wins": 0
            },
            {
                "tag": "JCCL9C2",
                "name": "LUDOPATEKU$$$$",
                "cardsEarned": 1400,
                "battlesPlayed": 1,
                "wins": 0
            },
            {
                "tag": "GPCR09JL",
                "name": "★★★COROCOTA★★★",
                "cardsEarned": 1189,
                "battlesPlayed": 1,
                "wins": 0
            },
            {
                "tag": "U2YCLPQU",
                "name": "er tabu",
                "cardsEarned": 1189,
                "battlesPlayed": 1,
                "wins": 0
            },
            {
                "tag": "2PR0999J",
                "name": "SEIMUS",
                "cardsEarned": 1189,
                "battlesPlayed": 1,
                "wins": 0
            },
            {
                "tag": "YCCQ8Q2Y",
                "name": "yonas",
                "cardsEarned": 1120,
                "battlesPlayed": 1,
                "wins": 0
            },
            {
                "tag": "LPC0GYY",
                "name": "Golem de agua",
                "cardsEarned": 1120,
                "battlesPlayed": 1,
                "wins": 0
            },
            {
                "tag": "PVUVCPC2",
                "name": "Adrigm97",
                "cardsEarned": 1120,
                "battlesPlayed": 1,
                "wins": 0
            },
            {
                "tag": "PYRL2CLU",
                "name": "ABEL",
                "cardsEarned": 1120,
                "battlesPlayed": 1,
                "wins": 0
            },
            {
                "tag": "22Q2RCJVV",
                "name": "GaliciaNoMapa",
                "cardsEarned": 1120,
                "battlesPlayed": 1,
                "wins": 0
            },
            {
                "tag": "2CR82YPG",
                "name": "sodein14",
                "cardsEarned": 1120,
                "battlesPlayed": 1,
                "wins": 0
            },
            {
                "tag": "2LLYC9PPC",
                "name": "sifh21",
                "cardsEarned": 1120,
                "battlesPlayed": 1,
                "wins": 0
            },
            {
                "tag": "8PQ9VVPG8",
                "name": "Sanxsp",
                "cardsEarned": 1120,
                "battlesPlayed": 1,
                "wins": 0
            },
            {
                "tag": "28GCCQ2L",
                "name": "maitee",
                "cardsEarned": 560,
                "battlesPlayed": 1,
                "wins": 0
            }
        ],
        "standings": [
            {
                "tag": "9GJ90JV",
                "name": "成都今夜请将我遗忘",
                "participants": 43,
                "battlesPlayed": 43,
                "wins": 30,
                "crowns": 55,
                "warTrophies": 2627,
                "warTrophiesChange": 130,
                "badge": {
                    "name": "Diamond_Star_01",
                    "category": "01_Symbol",
                    "id": 16000020,
                    "image": "https://royaleapi.github.io/cr-api-assets/badges/Diamond_Star_01.png"
                }
            },
            {
                "tag": "GRJJRGV",
                "name": "红包营",
                "participants": 35,
                "battlesPlayed": 43,
                "wins": 29,
                "crowns": 48,
                "warTrophies": 3067,
                "warTrophiesChange": 79,
                "badge": {
                    "name": "Coin_02",
                    "category": "03_Royale",
                    "id": 16000103,
                    "image": "https://royaleapi.github.io/cr-api-assets/badges/Coin_02.png"
                }
            },
            {
                "tag": "2C2QYCRL",
                "name": "MiSt",
                "participants": 39,
                "battlesPlayed": 42,
                "wins": 26,
                "crowns": 47,
                "warTrophies": 2041,
                "warTrophiesChange": 1,
                "badge": {
                    "name": "Skull_06",
                    "category": "01_Symbol",
                    "id": 16000029,
                    "image": "https://royaleapi.github.io/cr-api-assets/badges/Skull_06.png"
                }
            },
            {
                "tag": "PPYPCV",
                "name": "INVERNALIA",
                "participants": 44,
                "battlesPlayed": 44,
                "wins": 21,
                "crowns": 38,
                "warTrophies": 2927,
                "warTrophiesChange": -29,
                "badge": {
                    "name": "Freeze_01",
                    "category": "01_Symbol",
                    "id": 16000168,
                    "image": "https://royaleapi.github.io/cr-api-assets/badges/Freeze_01.png"
                }
            },
            {
                "tag": "9J2JVRQY",
                "name": "ATLAS",
                "participants": 43,
                "battlesPlayed": 40,
                "wins": 14,
                "crowns": 28,
                "warTrophies": 2528,
                "warTrophiesChange": -86,
                "badge": {
                    "name": "flag_k_04",
                    "category": "02_Flag",
                    "id": 16000081,
                    "image": "https://royaleapi.github.io/cr-api-assets/badges/flag_k_04.png"
                }
            }
        ],
        "seasonNumber": 10
    },
    {
        "createdDate": 1535203324,
        "participants": [
            {
                "tag": "2GQGUGQQ8",
                "name": "jmpv",
                "cardsEarned": 2640,
                "battlesPlayed": 2,
                "wins": 2
            },
            {
                "tag": "YLL2YG0Q",
                "name": "SeRmAo",
                "cardsEarned": 2337,
                "battlesPlayed": 2,
                "wins": 1
            },
            {
                "tag": "2Y22P9V28",
                "name": "⚽cristianO_vk⚽",
                "cardsEarned": 1760,
                "battlesPlayed": 2,
                "wins": 1
            },
            {
                "tag": "YR8LLUU8",
                "name": "ELVIS",
                "cardsEarned": 1760,
                "battlesPlayed": 2,
                "wins": 1
            },
            {
                "tag": "PYQC8C2G",
                "name": "Rayan",
                "cardsEarned": 1320,
                "battlesPlayed": 2,
                "wins": 1
            },
            {
                "tag": "U2YCLPQU",
                "name": "er tabu",
                "cardsEarned": 2337,
                "battlesPlayed": 2,
                "wins": 0
            },
            {
                "tag": "2VC9GULRV",
                "name": "joker",
                "cardsEarned": 1760,
                "battlesPlayed": 2,
                "wins": 0
            },
            {
                "tag": "2LLYC9PPC",
                "name": "sifh21",
                "cardsEarned": 1760,
                "battlesPlayed": 2,
                "wins": 0
            },
            {
                "tag": "28GCCQ2L",
                "name": "maitee",
                "cardsEarned": 1320,
                "battlesPlayed": 2,
                "wins": 0
            },
            {
                "tag": "90GP0RQ9",
                "name": "vulosky",
                "cardsEarned": 2337,
                "battlesPlayed": 1,
                "wins": 1
            },
            {
                "tag": "PYL8PPJ8",
                "name": "Ragnar",
                "cardsEarned": 2200,
                "battlesPlayed": 1,
                "wins": 1
            },
            {
                "tag": "JJP9JUG2",
                "name": "#Núñez",
                "cardsEarned": 2200,
                "battlesPlayed": 1,
                "wins": 1
            },
            {
                "tag": "9CCLRG9Y",
                "name": "lebowsky",
                "cardsEarned": 2200,
                "battlesPlayed": 1,
                "wins": 1
            },
            {
                "tag": "GPCR09JL",
                "name": "★★★COROCOTA★★★",
                "cardsEarned": 1869,
                "battlesPlayed": 1,
                "wins": 1
            },
            {
                "tag": "PVUVCPC2",
                "name": "Adrigm97",
                "cardsEarned": 1760,
                "battlesPlayed": 1,
                "wins": 1
            },
            {
                "tag": "8P9LV88Y",
                "name": "guyur",
                "cardsEarned": 1320,
                "battlesPlayed": 1,
                "wins": 1
            },
            {
                "tag": "Q0JUQLRR",
                "name": "kalico",
                "cardsEarned": 2695,
                "battlesPlayed": 1,
                "wins": 0
            },
            {
                "tag": "QPCJQJQ",
                "name": "Jimlou",
                "cardsEarned": 2640,
                "battlesPlayed": 1,
                "wins": 0
            },
            {
                "tag": "2PR0999J",
                "name": "SEIMUS",
                "cardsEarned": 2337,
                "battlesPlayed": 1,
                "wins": 0
            },
            {
                "tag": "UJPG9Q2J",
                "name": "Des",
                "cardsEarned": 2200,
                "battlesPlayed": 1,
                "wins": 0
            },
            {
                "tag": "U08RLC2U",
                "name": "Nayper",
                "cardsEarned": 2200,
                "battlesPlayed": 1,
                "wins": 0
            },
            {
                "tag": "88LQUVPG",
                "name": "Asten",
                "cardsEarned": 2200,
                "battlesPlayed": 1,
                "wins": 0
            },
            {
                "tag": "8UVPJ8UR",
                "name": "Mikel",
                "cardsEarned": 2200,
                "battlesPlayed": 1,
                "wins": 0
            },
            {
                "tag": "RVV82QP",
                "name": "Guilliton",
                "cardsEarned": 2200,
                "battlesPlayed": 1,
                "wins": 0
            },
            {
                "tag": "CQ82YRR8",
                "name": "Adrián M",
                "cardsEarned": 1980,
                "battlesPlayed": 1,
                "wins": 0
            },
            {
                "tag": "2290RRUC9",
                "name": "Ivan",
                "cardsEarned": 1869,
                "battlesPlayed": 1,
                "wins": 0
            },
            {
                "tag": "2QJU9P2J",
                "name": "PABLOMIZZ",
                "cardsEarned": 1760,
                "battlesPlayed": 1,
                "wins": 0
            },
            {
                "tag": "8GVJYQ8G",
                "name": "fruticola",
                "cardsEarned": 1760,
                "battlesPlayed": 1,
                "wins": 0
            },
            {
                "tag": "2P89Q88G",
                "name": "SergileTTe",
                "cardsEarned": 1760,
                "battlesPlayed": 1,
                "wins": 0
            },
            {
                "tag": "8G8GCQRJ",
                "name": "Mikky",
                "cardsEarned": 1760,
                "battlesPlayed": 1,
                "wins": 0
            },
            {
                "tag": "22Q2RCJVV",
                "name": "GaliciaNoMapa",
                "cardsEarned": 1760,
                "battlesPlayed": 1,
                "wins": 0
            },
            {
                "tag": "9LCY9Y2J",
                "name": "Mosquito23",
                "cardsEarned": 1760,
                "battlesPlayed": 1,
                "wins": 0
            },
            {
                "tag": "G9J9CPL",
                "name": "S.J.P",
                "cardsEarned": 1401,
                "battlesPlayed": 1,
                "wins": 0
            },
            {
                "tag": "2CR82YPG",
                "name": "sodein14",
                "cardsEarned": 1320,
                "battlesPlayed": 1,
                "wins": 0
            },
            {
                "tag": "88L9GPPR",
                "name": "♠♥♦♣BooooMZ♣♦♥♠",
                "cardsEarned": 1320,
                "battlesPlayed": 1,
                "wins": 0
            },
            {
                "tag": "2QVQUCP",
                "name": "Maty97ro",
                "cardsEarned": 1760,
                "battlesPlayed": 0,
                "wins": 0
            }
        ],
        "standings": [
            {
                "tag": "LY9U2C",
                "name": "Aussie Royale",
                "participants": 46,
                "battlesPlayed": 45,
                "wins": 24,
                "crowns": 34,
                "warTrophies": 3441,
                "warTrophiesChange": 124,
                "badge": {
                    "name": "flag_b_03",
                    "category": "02_Flag",
                    "id": 16000053,
                    "image": "https://royaleapi.github.io/cr-api-assets/badges/flag_b_03.png"
                }
            },
            {
                "tag": "2Y0VQYCL",
                "name": "La Squadra",
                "participants": 38,
                "battlesPlayed": 44,
                "wins": 22,
                "crowns": 35,
                "warTrophies": 3083,
                "warTrophiesChange": 72,
                "badge": {
                    "name": "Gem_01",
                    "category": "03_Royale",
                    "id": 16000098,
                    "image": "https://royaleapi.github.io/cr-api-assets/badges/Gem_01.png"
                }
            },
            {
                "tag": "2LQRCP0",
                "name": "Sbaulatori",
                "participants": 38,
                "battlesPlayed": 45,
                "wins": 21,
                "crowns": 30,
                "warTrophies": 3058,
                "warTrophiesChange": -4,
                "badge": {
                    "name": "flag_k_04",
                    "category": "02_Flag",
                    "id": 16000081,
                    "image": "https://royaleapi.github.io/cr-api-assets/badges/flag_k_04.png"
                }
            },
            {
                "tag": "U0RCJ02",
                "name": "zanager forces",
                "participants": 30,
                "battlesPlayed": 43,
                "wins": 15,
                "crowns": 26,
                "warTrophies": 2972,
                "warTrophiesChange": -35,
                "badge": {
                    "name": "Bamboo_04",
                    "category": "01_Symbol",
                    "id": 16000139,
                    "image": "https://royaleapi.github.io/cr-api-assets/badges/Bamboo_04.png"
                }
            },
            {
                "tag": "PPYPCV",
                "name": "INVERNALIA",
                "participants": 36,
                "battlesPlayed": 44,
                "wins": 13,
                "crowns": 17,
                "warTrophies": 2956,
                "warTrophiesChange": -87,
                "badge": {
                    "name": "Freeze_01",
                    "category": "01_Symbol",
                    "id": 16000168,
                    "image": "https://royaleapi.github.io/cr-api-assets/badges/Freeze_01.png"
                }
            }
        ],
        "seasonNumber": 10
    },
    {
        "createdDate": 1535030287,
        "participants": [
            {
                "tag": "2PR0999J",
                "name": "SEIMUS",
                "cardsEarned": 2805,
                "battlesPlayed": 1,
                "wins": 1
            },
            {
                "tag": "G9J9CPL",
                "name": "S.J.P",
                "cardsEarned": 2337,
                "battlesPlayed": 1,
                "wins": 1
            },
            {
                "tag": "R9QRPYGL",
                "name": "KLOK",
                "cardsEarned": 2337,
                "battlesPlayed": 1,
                "wins": 1
            },
            {
                "tag": "2QJU9P2J",
                "name": "PABLOMIZZ",
                "cardsEarned": 2200,
                "battlesPlayed": 1,
                "wins": 1
            },
            {
                "tag": "GPCR09JL",
                "name": "★★★COROCOTA★★★",
                "cardsEarned": 2200,
                "battlesPlayed": 1,
                "wins": 1
            },
            {
                "tag": "2VC9GULRV",
                "name": "joker",
                "cardsEarned": 2200,
                "battlesPlayed": 1,
                "wins": 1
            },
            {
                "tag": "2908VYGYV",
                "name": ":)DAVIDS:)",
                "cardsEarned": 2200,
                "battlesPlayed": 1,
                "wins": 1
            },
            {
                "tag": "CRLRUQL",
                "name": "vrslm24",
                "cardsEarned": 1869,
                "battlesPlayed": 1,
                "wins": 1
            },
            {
                "tag": "YLL2YG0Q",
                "name": "SeRmAo",
                "cardsEarned": 1869,
                "battlesPlayed": 1,
                "wins": 1
            },
            {
                "tag": "CQ82YRR8",
                "name": "Adrián M",
                "cardsEarned": 1869,
                "battlesPlayed": 1,
                "wins": 1
            },
            {
                "tag": "JJP9JUG2",
                "name": "#Núñez",
                "cardsEarned": 1869,
                "battlesPlayed": 1,
                "wins": 1
            },
            {
                "tag": "8GV0Q9Q9",
                "name": "sergiomont yout",
                "cardsEarned": 1760,
                "battlesPlayed": 1,
                "wins": 1
            },
            {
                "tag": "VV9CRQ9U",
                "name": "MiriamBlade",
                "cardsEarned": 1760,
                "battlesPlayed": 1,
                "wins": 1
            },
            {
                "tag": "9CCLRG9Y",
                "name": "lebowsky",
                "cardsEarned": 1760,
                "battlesPlayed": 1,
                "wins": 1
            },
            {
                "tag": "YR8LLUU8",
                "name": "ELVIS",
                "cardsEarned": 1760,
                "battlesPlayed": 1,
                "wins": 1
            },
            {
                "tag": "QPCJQJQ",
                "name": "Jimlou",
                "cardsEarned": 1320,
                "battlesPlayed": 1,
                "wins": 1
            },
            {
                "tag": "9UCPVYP",
                "name": "⚡CANTON⚡",
                "cardsEarned": 1320,
                "battlesPlayed": 1,
                "wins": 1
            },
            {
                "tag": "2290RRUC9",
                "name": "Ivan",
                "cardsEarned": 2805,
                "battlesPlayed": 1,
                "wins": 0
            },
            {
                "tag": "Q0JUQLRR",
                "name": "kalico",
                "cardsEarned": 2640,
                "battlesPlayed": 1,
                "wins": 0
            },
            {
                "tag": "2QVQUCP",
                "name": "Maty97ro",
                "cardsEarned": 2640,
                "battlesPlayed": 1,
                "wins": 0
            },
            {
                "tag": "88LQUVPG",
                "name": "Asten",
                "cardsEarned": 2640,
                "battlesPlayed": 1,
                "wins": 0
            },
            {
                "tag": "RVV82QP",
                "name": "Guilliton",
                "cardsEarned": 2640,
                "battlesPlayed": 1,
                "wins": 0
            },
            {
                "tag": "2GQGUGQQ8",
                "name": "jmpv",
                "cardsEarned": 2640,
                "battlesPlayed": 1,
                "wins": 0
            },
            {
                "tag": "298R9CCUL",
                "name": "floky",
                "cardsEarned": 2337,
                "battlesPlayed": 1,
                "wins": 0
            },
            {
                "tag": "2P89Q88G",
                "name": "SergileTTe",
                "cardsEarned": 2200,
                "battlesPlayed": 1,
                "wins": 0
            },
            {
                "tag": "2Y22P9V28",
                "name": "⚽cristianO_vk⚽",
                "cardsEarned": 2200,
                "battlesPlayed": 1,
                "wins": 0
            },
            {
                "tag": "9LCY9Y2J",
                "name": "Mosquito23",
                "cardsEarned": 2200,
                "battlesPlayed": 1,
                "wins": 0
            },
            {
                "tag": "PYQC8C2G",
                "name": "Rayan",
                "cardsEarned": 2200,
                "battlesPlayed": 1,
                "wins": 0
            },
            {
                "tag": "9LL99RRU",
                "name": "Drareg",
                "cardsEarned": 2200,
                "battlesPlayed": 1,
                "wins": 0
            },
            {
                "tag": "28GCCQ2L",
                "name": "maitee",
                "cardsEarned": 2200,
                "battlesPlayed": 1,
                "wins": 0
            },
            {
                "tag": "2CR82YPG",
                "name": "sodein14",
                "cardsEarned": 2200,
                "battlesPlayed": 1,
                "wins": 0
            },
            {
                "tag": "8P9LV88Y",
                "name": "guyur",
                "cardsEarned": 1869,
                "battlesPlayed": 1,
                "wins": 0
            },
            {
                "tag": "8G8GCQRJ",
                "name": "Mikky",
                "cardsEarned": 1869,
                "battlesPlayed": 1,
                "wins": 0
            },
            {
                "tag": "LJVJ992Q",
                "name": "Jaaviix",
                "cardsEarned": 1869,
                "battlesPlayed": 1,
                "wins": 0
            },
            {
                "tag": "8GVJYQ8G",
                "name": "fruticola",
                "cardsEarned": 1760,
                "battlesPlayed": 1,
                "wins": 0
            },
            {
                "tag": "U08RLC2U",
                "name": "Nayper",
                "cardsEarned": 1760,
                "battlesPlayed": 1,
                "wins": 0
            },
            {
                "tag": "PVUVCPC2",
                "name": "Adrigm97",
                "cardsEarned": 1760,
                "battlesPlayed": 1,
                "wins": 0
            },
            {
                "tag": "PYL8PPJ8",
                "name": "Ragnar",
                "cardsEarned": 1760,
                "battlesPlayed": 1,
                "wins": 0
            },
            {
                "tag": "22Q2RCJVV",
                "name": "GaliciaNoMapa",
                "cardsEarned": 1760,
                "battlesPlayed": 1,
                "wins": 0
            },
            {
                "tag": "208YYCQC",
                "name": "SergrogIX",
                "cardsEarned": 1760,
                "battlesPlayed": 1,
                "wins": 0
            },
            {
                "tag": "2LLYC9PPC",
                "name": "sifh21",
                "cardsEarned": 1760,
                "battlesPlayed": 1,
                "wins": 0
            },
            {
                "tag": "8UVPJ8UR",
                "name": "Mikel",
                "cardsEarned": 1760,
                "battlesPlayed": 1,
                "wins": 0
            },
            {
                "tag": "90GP0RQ9",
                "name": "vulosky",
                "cardsEarned": 1401,
                "battlesPlayed": 1,
                "wins": 0
            },
            {
                "tag": "UJPG9Q2J",
                "name": "Des",
                "cardsEarned": 1320,
                "battlesPlayed": 1,
                "wins": 0
            },
            {
                "tag": "LPC0GYY",
                "name": "Golem de agua",
                "cardsEarned": 880,
                "battlesPlayed": 1,
                "wins": 0
            },
            {
                "tag": "88L9GPPR",
                "name": "♠♥♦♣BooooMZ♣♦♥♠",
                "cardsEarned": 1760,
                "battlesPlayed": 0,
                "wins": 0
            }
        ],
        "standings": [
            {
                "tag": "JJP0GQ2",
                "name": "100万円までは無課金",
                "participants": 44,
                "battlesPlayed": 42,
                "wins": 28,
                "crowns": 45,
                "warTrophies": 3507,
                "warTrophiesChange": 128,
                "badge": {
                    "name": "Flame_03",
                    "category": "01_Symbol",
                    "id": 16000002,
                    "image": "https://royaleapi.github.io/cr-api-assets/badges/Flame_03.png"
                }
            },
            {
                "tag": "UU9L",
                "name": "きくすいコーポ",
                "participants": 45,
                "battlesPlayed": 46,
                "wins": 27,
                "crowns": 45,
                "warTrophies": 4246,
                "warTrophiesChange": 77,
                "badge": {
                    "name": "Fan_04",
                    "category": "01_Symbol",
                    "id": 16000119,
                    "image": "https://royaleapi.github.io/cr-api-assets/badges/Fan_04.png"
                }
            },
            {
                "tag": "PPYPCV",
                "name": "INVERNALIA",
                "participants": 46,
                "battlesPlayed": 45,
                "wins": 17,
                "crowns": 32,
                "warTrophies": 3043,
                "warTrophiesChange": -8,
                "badge": {
                    "name": "Freeze_01",
                    "category": "01_Symbol",
                    "id": 16000168,
                    "image": "https://royaleapi.github.io/cr-api-assets/badges/Freeze_01.png"
                }
            },
            {
                "tag": "QCYQY9",
                "name": "부산",
                "participants": 43,
                "battlesPlayed": 46,
                "wins": 16,
                "crowns": 23,
                "warTrophies": 2988,
                "warTrophiesChange": -34,
                "badge": {
                    "name": "Fan_01",
                    "category": "01_Symbol",
                    "id": 16000116,
                    "image": "https://royaleapi.github.io/cr-api-assets/badges/Fan_01.png"
                }
            },
            {
                "tag": "2Y0VQYCL",
                "name": "La Squadra",
                "participants": 44,
                "battlesPlayed": 46,
                "wins": 12,
                "crowns": 24,
                "warTrophies": 3011,
                "warTrophiesChange": -88,
                "badge": {
                    "name": "Gem_01",
                    "category": "03_Royale",
                    "id": 16000098,
                    "image": "https://royaleapi.github.io/cr-api-assets/badges/Gem_01.png"
                }
            }
        ],
        "seasonNumber": 10
    },
    {
        "createdDate": 1534856767,
        "participants": [
            {
                "tag": "22Q2RCJVV",
                "name": "GaliciaNoMapa",
                "cardsEarned": 1785,
                "battlesPlayed": 1,
                "wins": 1
            },
            {
                "tag": "2QJU9P2J",
                "name": "PABLOMIZZ",
                "cardsEarned": 1680,
                "battlesPlayed": 1,
                "wins": 1
            },
            {
                "tag": "PYL8PPJ8",
                "name": "Ragnar",
                "cardsEarned": 1680,
                "battlesPlayed": 1,
                "wins": 1
            },
            {
                "tag": "88L9GPPR",
                "name": "♠♥♦♣BooooMZ♣♦♥♠",
                "cardsEarned": 1680,
                "battlesPlayed": 1,
                "wins": 1
            },
            {
                "tag": "CRLRUQL",
                "name": "vrslm24",
                "cardsEarned": 1487,
                "battlesPlayed": 1,
                "wins": 1
            },
            {
                "tag": "YLL2YG0Q",
                "name": "SeRmAo",
                "cardsEarned": 1487,
                "battlesPlayed": 1,
                "wins": 1
            },
            {
                "tag": "2Y22P9V28",
                "name": "⚽cristianO_vk⚽",
                "cardsEarned": 1487,
                "battlesPlayed": 1,
                "wins": 1
            },
            {
                "tag": "CQ82YRR8",
                "name": "Adrián M",
                "cardsEarned": 1487,
                "battlesPlayed": 1,
                "wins": 1
            },
            {
                "tag": "8G8GCQRJ",
                "name": "Mikky",
                "cardsEarned": 1487,
                "battlesPlayed": 1,
                "wins": 1
            },
            {
                "tag": "90GP0RQ9",
                "name": "vulosky",
                "cardsEarned": 1487,
                "battlesPlayed": 1,
                "wins": 1
            },
            {
                "tag": "R9QRPYGL",
                "name": "KLOK",
                "cardsEarned": 1400,
                "battlesPlayed": 1,
                "wins": 1
            },
            {
                "tag": "Q0JUQLRR",
                "name": "kalico",
                "cardsEarned": 1400,
                "battlesPlayed": 1,
                "wins": 1
            },
            {
                "tag": "2VC9GULRV",
                "name": "joker",
                "cardsEarned": 1400,
                "battlesPlayed": 1,
                "wins": 1
            },
            {
                "tag": "PVUVCPC2",
                "name": "Adrigm97",
                "cardsEarned": 1400,
                "battlesPlayed": 1,
                "wins": 1
            },
            {
                "tag": "2908VYGYV",
                "name": ":)DAVIDS:)",
                "cardsEarned": 1400,
                "battlesPlayed": 1,
                "wins": 1
            },
            {
                "tag": "88LQUVPG",
                "name": "Asten",
                "cardsEarned": 1400,
                "battlesPlayed": 1,
                "wins": 1
            },
            {
                "tag": "PYQC8C2G",
                "name": "Rayan",
                "cardsEarned": 1400,
                "battlesPlayed": 1,
                "wins": 1
            },
            {
                "tag": "8UVPJ8UR",
                "name": "Mikel",
                "cardsEarned": 1400,
                "battlesPlayed": 1,
                "wins": 1
            },
            {
                "tag": "G9J9CPL",
                "name": "S.J.P",
                "cardsEarned": 1189,
                "battlesPlayed": 1,
                "wins": 1
            },
            {
                "tag": "LJVJ992Q",
                "name": "Jaaviix",
                "cardsEarned": 1189,
                "battlesPlayed": 1,
                "wins": 1
            },
            {
                "tag": "298R9CCUL",
                "name": "floky",
                "cardsEarned": 1120,
                "battlesPlayed": 1,
                "wins": 1
            },
            {
                "tag": "8GVJYQ8G",
                "name": "fruticola",
                "cardsEarned": 1120,
                "battlesPlayed": 1,
                "wins": 1
            },
            {
                "tag": "8GV0Q9Q9",
                "name": "sergiomont yout",
                "cardsEarned": 1120,
                "battlesPlayed": 1,
                "wins": 1
            },
            {
                "tag": "2P89Q88G",
                "name": "SergileTTe",
                "cardsEarned": 1120,
                "battlesPlayed": 1,
                "wins": 1
            },
            {
                "tag": "2CR82YPG",
                "name": "sodein14",
                "cardsEarned": 1120,
                "battlesPlayed": 1,
                "wins": 1
            },
            {
                "tag": "LPC0GYY",
                "name": "Golem de agua",
                "cardsEarned": 840,
                "battlesPlayed": 1,
                "wins": 1
            },
            {
                "tag": "PYRL2CLU",
                "name": "ABEL",
                "cardsEarned": 840,
                "battlesPlayed": 1,
                "wins": 1
            },
            {
                "tag": "9LL99RRU",
                "name": "Drareg",
                "cardsEarned": 840,
                "battlesPlayed": 1,
                "wins": 1
            },
            {
                "tag": "2GQGUGQQ8",
                "name": "jmpv",
                "cardsEarned": 840,
                "battlesPlayed": 1,
                "wins": 1
            },
            {
                "tag": "QPCJQJQ",
                "name": "Jimlou",
                "cardsEarned": 1400,
                "battlesPlayed": 1,
                "wins": 0
            },
            {
                "tag": "9LCY9Y2J",
                "name": "Mosquito23",
                "cardsEarned": 1400,
                "battlesPlayed": 1,
                "wins": 0
            },
            {
                "tag": "28GCCQ2L",
                "name": "maitee",
                "cardsEarned": 1400,
                "battlesPlayed": 1,
                "wins": 0
            },
            {
                "tag": "2LLYC9PPC",
                "name": "sifh21",
                "cardsEarned": 1400,
                "battlesPlayed": 1,
                "wins": 0
            },
            {
                "tag": "2PR0999J",
                "name": "SEIMUS",
                "cardsEarned": 1400,
                "battlesPlayed": 1,
                "wins": 0
            },
            {
                "tag": "8P9LV88Y",
                "name": "guyur",
                "cardsEarned": 1189,
                "battlesPlayed": 1,
                "wins": 0
            },
            {
                "tag": "YVY0UQ22",
                "name": "GEO_NATHA",
                "cardsEarned": 1120,
                "battlesPlayed": 1,
                "wins": 0
            },
            {
                "tag": "2290RRUC9",
                "name": "Ivan",
                "cardsEarned": 1120,
                "battlesPlayed": 1,
                "wins": 0
            },
            {
                "tag": "GPCR09JL",
                "name": "★★★COROCOTA★★★",
                "cardsEarned": 1120,
                "battlesPlayed": 1,
                "wins": 0
            },
            {
                "tag": "9UCPVYP",
                "name": "⚡CANTON⚡",
                "cardsEarned": 1120,
                "battlesPlayed": 1,
                "wins": 0
            },
            {
                "tag": "UJPG9Q2J",
                "name": "Des",
                "cardsEarned": 1120,
                "battlesPlayed": 1,
                "wins": 0
            },
            {
                "tag": "VV9CRQ9U",
                "name": "MiriamBlade",
                "cardsEarned": 1120,
                "battlesPlayed": 1,
                "wins": 0
            },
            {
                "tag": "JJP9JUG2",
                "name": "#Núñez",
                "cardsEarned": 1120,
                "battlesPlayed": 1,
                "wins": 0
            },
            {
                "tag": "208YYCQC",
                "name": "SergrogIX",
                "cardsEarned": 1120,
                "battlesPlayed": 1,
                "wins": 0
            },
            {
                "tag": "YR8LLUU8",
                "name": "ELVIS",
                "cardsEarned": 1120,
                "battlesPlayed": 1,
                "wins": 0
            },
            {
                "tag": "RVV82QP",
                "name": "Guilliton",
                "cardsEarned": 1120,
                "battlesPlayed": 1,
                "wins": 0
            },
            {
                "tag": "2QVQUCP",
                "name": "Maty97ro",
                "cardsEarned": 840,
                "battlesPlayed": 1,
                "wins": 0
            }
        ],
        "standings": [
            {
                "tag": "9R2GLJ0Y",
                "name": "KSA",
                "participants": 45,
                "battlesPlayed": 46,
                "wins": 30,
                "crowns": 53,
                "warTrophies": 2482,
                "warTrophiesChange": 130,
                "badge": {
                    "name": "Bamboo_01",
                    "category": "01_Symbol",
                    "id": 16000136,
                    "image": "https://royaleapi.github.io/cr-api-assets/badges/Bamboo_01.png"
                }
            },
            {
                "tag": "PPYPCV",
                "name": "INVERNALIA",
                "participants": 46,
                "battlesPlayed": 46,
                "wins": 29,
                "crowns": 57,
                "warTrophies": 3051,
                "warTrophiesChange": 79,
                "badge": {
                    "name": "Freeze_01",
                    "category": "01_Symbol",
                    "id": 16000168,
                    "image": "https://royaleapi.github.io/cr-api-assets/badges/Freeze_01.png"
                }
            },
            {
                "tag": "2280QQU",
                "name": "愛知県JAPAN",
                "participants": 42,
                "battlesPlayed": 45,
                "wins": 24,
                "crowns": 41,
                "warTrophies": 2566,
                "warTrophiesChange": -1,
                "badge": {
                    "name": "Flame_01",
                    "category": "01_Symbol",
                    "id": 16000000,
                    "image": "https://royaleapi.github.io/cr-api-assets/badges/Flame_01.png"
                }
            },
            {
                "tag": "889YVPYR",
                "name": "50 kings",
                "participants": 43,
                "battlesPlayed": 45,
                "wins": 23,
                "crowns": 38,
                "warTrophies": 1992,
                "warTrophiesChange": -27,
                "badge": {
                    "name": "Traditional_Star_02",
                    "category": "01_Symbol",
                    "id": 16000037,
                    "image": "https://royaleapi.github.io/cr-api-assets/badges/Traditional_Star_02.png"
                }
            },
            {
                "tag": "8YRGV09L",
                "name": "BRASIL PUSH",
                "participants": 43,
                "battlesPlayed": 42,
                "wins": 17,
                "crowns": 29,
                "warTrophies": 2400,
                "warTrophiesChange": -83,
                "badge": {
                    "name": "Skull_01",
                    "category": "01_Symbol",
                    "id": 16000024,
                    "image": "https://royaleapi.github.io/cr-api-assets/badges/Skull_01.png"
                }
            }
        ],
        "seasonNumber": 9
    },
    {
        "createdDate": 1534683881,
        "participants": [
            {
                "tag": "2CR82YPG",
                "name": "sodein14",
                "cardsEarned": 1400,
                "battlesPlayed": 2,
                "wins": 1
            },
            {
                "tag": "90GP0RQ9",
                "name": "vulosky",
                "cardsEarned": 1785,
                "battlesPlayed": 1,
                "wins": 1
            },
            {
                "tag": "9LCY9Y2J",
                "name": "Mosquito23",
                "cardsEarned": 1680,
                "battlesPlayed": 1,
                "wins": 1
            },
            {
                "tag": "2GQGUGQQ8",
                "name": "jmpv",
                "cardsEarned": 1680,
                "battlesPlayed": 1,
                "wins": 1
            },
            {
                "tag": "CRLRUQL",
                "name": "vrslm24",
                "cardsEarned": 1487,
                "battlesPlayed": 1,
                "wins": 1
            },
            {
                "tag": "YLL2YG0Q",
                "name": "SeRmAo",
                "cardsEarned": 1487,
                "battlesPlayed": 1,
                "wins": 1
            },
            {
                "tag": "CQ82YRR8",
                "name": "Adrián M",
                "cardsEarned": 1487,
                "battlesPlayed": 1,
                "wins": 1
            },
            {
                "tag": "LJVJ992Q",
                "name": "Jaaviix",
                "cardsEarned": 1487,
                "battlesPlayed": 1,
                "wins": 1
            },
            {
                "tag": "9CCLRG9Y",
                "name": "lebowsky",
                "cardsEarned": 1487,
                "battlesPlayed": 1,
                "wins": 1
            },
            {
                "tag": "8GV0Q9Q9",
                "name": "sergiomont yout",
                "cardsEarned": 1400,
                "battlesPlayed": 1,
                "wins": 1
            },
            {
                "tag": "22Q2RCJVV",
                "name": "GaliciaNoMapa",
                "cardsEarned": 1400,
                "battlesPlayed": 1,
                "wins": 1
            },
            {
                "tag": "2PU9202V",
                "name": "YourFkngGranpa",
                "cardsEarned": 1400,
                "battlesPlayed": 1,
                "wins": 1
            },
            {
                "tag": "PYQC8C2G",
                "name": "Rayan",
                "cardsEarned": 1400,
                "battlesPlayed": 1,
                "wins": 1
            },
            {
                "tag": "88L9GPPR",
                "name": "♠♥♦♣BooooMZ♣♦♥♠",
                "cardsEarned": 1400,
                "battlesPlayed": 1,
                "wins": 1
            },
            {
                "tag": "PYRL2CLU",
                "name": "ABEL",
                "cardsEarned": 1400,
                "battlesPlayed": 1,
                "wins": 1
            },
            {
                "tag": "8G8GCQRJ",
                "name": "Mikky",
                "cardsEarned": 1400,
                "battlesPlayed": 1,
                "wins": 1
            },
            {
                "tag": "RVV82QP",
                "name": "Guilliton",
                "cardsEarned": 1400,
                "battlesPlayed": 1,
                "wins": 1
            },
            {
                "tag": "2PR0999J",
                "name": "SEIMUS",
                "cardsEarned": 1400,
                "battlesPlayed": 1,
                "wins": 1
            },
            {
                "tag": "G9J9CPL",
                "name": "S.J.P",
                "cardsEarned": 1189,
                "battlesPlayed": 1,
                "wins": 1
            },
            {
                "tag": "PYL8PPJ8",
                "name": "Ragnar",
                "cardsEarned": 1120,
                "battlesPlayed": 1,
                "wins": 1
            },
            {
                "tag": "2908VYGYV",
                "name": ":)DAVIDS:)",
                "cardsEarned": 1120,
                "battlesPlayed": 1,
                "wins": 1
            },
            {
                "tag": "LPC0GYY",
                "name": "Golem de agua",
                "cardsEarned": 1120,
                "battlesPlayed": 1,
                "wins": 1
            },
            {
                "tag": "JJP9JUG2",
                "name": "#Núñez",
                "cardsEarned": 1120,
                "battlesPlayed": 1,
                "wins": 1
            },
            {
                "tag": "208YYCQC",
                "name": "SergrogIX",
                "cardsEarned": 1120,
                "battlesPlayed": 1,
                "wins": 1
            },
            {
                "tag": "2LLYC9PPC",
                "name": "sifh21",
                "cardsEarned": 1120,
                "battlesPlayed": 1,
                "wins": 1
            },
            {
                "tag": "GPCR09JL",
                "name": "★★★COROCOTA★★★",
                "cardsEarned": 840,
                "battlesPlayed": 1,
                "wins": 1
            },
            {
                "tag": "8GVJYQ8G",
                "name": "fruticola",
                "cardsEarned": 840,
                "battlesPlayed": 1,
                "wins": 1
            },
            {
                "tag": "2VC9GULRV",
                "name": "joker",
                "cardsEarned": 840,
                "battlesPlayed": 1,
                "wins": 1
            },
            {
                "tag": "8P9LV88Y",
                "name": "guyur",
                "cardsEarned": 840,
                "battlesPlayed": 1,
                "wins": 1
            },
            {
                "tag": "2QVQUCP",
                "name": "Maty97ro",
                "cardsEarned": 840,
                "battlesPlayed": 1,
                "wins": 1
            },
            {
                "tag": "UJPG9Q2J",
                "name": "Des",
                "cardsEarned": 280,
                "battlesPlayed": 1,
                "wins": 1
            },
            {
                "tag": "QPCJQJQ",
                "name": "Jimlou",
                "cardsEarned": 1680,
                "battlesPlayed": 1,
                "wins": 0
            },
            {
                "tag": "Q0JUQLRR",
                "name": "kalico",
                "cardsEarned": 1400,
                "battlesPlayed": 1,
                "wins": 0
            },
            {
                "tag": "298R9CCUL",
                "name": "floky",
                "cardsEarned": 1400,
                "battlesPlayed": 1,
                "wins": 0
            },
            {
                "tag": "9UCPVYP",
                "name": "⚡CANTON⚡",
                "cardsEarned": 1400,
                "battlesPlayed": 1,
                "wins": 0
            },
            {
                "tag": "2P89Q88G",
                "name": "SergileTTe",
                "cardsEarned": 1400,
                "battlesPlayed": 1,
                "wins": 0
            },
            {
                "tag": "PVUVCPC2",
                "name": "Adrigm97",
                "cardsEarned": 1400,
                "battlesPlayed": 1,
                "wins": 0
            },
            {
                "tag": "8UVPJ8UR",
                "name": "Mikel",
                "cardsEarned": 1400,
                "battlesPlayed": 1,
                "wins": 0
            },
            {
                "tag": "R9QRPYGL",
                "name": "KLOK",
                "cardsEarned": 1189,
                "battlesPlayed": 1,
                "wins": 0
            },
            {
                "tag": "2290RRUC9",
                "name": "Ivan",
                "cardsEarned": 1120,
                "battlesPlayed": 1,
                "wins": 0
            },
            {
                "tag": "2QJU9P2J",
                "name": "PABLOMIZZ",
                "cardsEarned": 1120,
                "battlesPlayed": 1,
                "wins": 0
            },
            {
                "tag": "U08RLC2U",
                "name": "Nayper",
                "cardsEarned": 1120,
                "battlesPlayed": 1,
                "wins": 0
            },
            {
                "tag": "VV9CRQ9U",
                "name": "MiriamBlade",
                "cardsEarned": 1120,
                "battlesPlayed": 1,
                "wins": 0
            },
            {
                "tag": "2Y22P9V28",
                "name": "⚽cristianO_vk⚽",
                "cardsEarned": 1120,
                "battlesPlayed": 1,
                "wins": 0
            },
            {
                "tag": "88LQUVPG",
                "name": "Asten",
                "cardsEarned": 840,
                "battlesPlayed": 1,
                "wins": 0
            },
            {
                "tag": "9LL99RRU",
                "name": "Drareg",
                "cardsEarned": 840,
                "battlesPlayed": 1,
                "wins": 0
            }
        ],
        "standings": [
            {
                "tag": "PPYPCV",
                "name": "INVERNALIA",
                "participants": 46,
                "battlesPlayed": 47,
                "wins": 31,
                "crowns": 57,
                "warTrophies": 2972,
                "warTrophiesChange": 131,
                "badge": {
                    "name": "Freeze_01",
                    "category": "01_Symbol",
                    "id": 16000168,
                    "image": "https://royaleapi.github.io/cr-api-assets/badges/Freeze_01.png"
                }
            },
            {
                "tag": "U8U8PV",
                "name": "台北 銀河總部",
                "participants": 47,
                "battlesPlayed": 47,
                "wins": 25,
                "crowns": 52,
                "warTrophies": 2481,
                "warTrophiesChange": 75,
                "badge": {
                    "name": "Heart_01",
                    "category": "01_Symbol",
                    "id": 16000108,
                    "image": "https://royaleapi.github.io/cr-api-assets/badges/Heart_01.png"
                }
            },
            {
                "tag": "RYRQ2L",
                "name": "愛媛JAPAN",
                "participants": 47,
                "battlesPlayed": 46,
                "wins": 24,
                "crowns": 38,
                "warTrophies": 2116,
                "warTrophiesChange": -1,
                "badge": {
                    "name": "flag_c_03",
                    "category": "02_Flag",
                    "id": 16000054,
                    "image": "https://royaleapi.github.io/cr-api-assets/badges/flag_c_03.png"
                }
            },
            {
                "tag": "2280QQU",
                "name": "愛知県JAPAN",
                "participants": 44,
                "battlesPlayed": 47,
                "wins": 21,
                "crowns": 32,
                "warTrophies": 2567,
                "warTrophiesChange": -29,
                "badge": {
                    "name": "Flame_01",
                    "category": "01_Symbol",
                    "id": 16000000,
                    "image": "https://royaleapi.github.io/cr-api-assets/badges/Flame_01.png"
                }
            },
            {
                "tag": "UJJL8G",
                "name": "Taiwan Island",
                "participants": 42,
                "battlesPlayed": 46,
                "wins": 18,
                "crowns": 29,
                "warTrophies": 2427,
                "warTrophiesChange": -82,
                "badge": {
                    "name": "Flame_03",
                    "category": "01_Symbol",
                    "id": 16000002,
                    "image": "https://royaleapi.github.io/cr-api-assets/badges/Flame_03.png"
                }
            }
        ],
        "seasonNumber": 9
    },
    {
        "createdDate": 1534510889,
        "participants": [
            {
                "tag": "QPCJQJQ",
                "name": "Jimlou",
                "cardsEarned": 1680,
                "battlesPlayed": 1,
                "wins": 1
            },
            {
                "tag": "2QVQUCP",
                "name": "Maty97ro",
                "cardsEarned": 1680,
                "battlesPlayed": 1,
                "wins": 1
            },
            {
                "tag": "Q9PJJYU8",
                "name": "SAID①⑦",
                "cardsEarned": 1487,
                "battlesPlayed": 1,
                "wins": 1
            },
            {
                "tag": "90GP0RQ9",
                "name": "vulosky",
                "cardsEarned": 1487,
                "battlesPlayed": 1,
                "wins": 1
            },
            {
                "tag": "2QJU9P2J",
                "name": "PABLOMIZZ",
                "cardsEarned": 1400,
                "battlesPlayed": 1,
                "wins": 1
            },
            {
                "tag": "YLL2YG0Q",
                "name": "SeRmAo",
                "cardsEarned": 1400,
                "battlesPlayed": 1,
                "wins": 1
            },
            {
                "tag": "LPC0GYY",
                "name": "Golem de agua",
                "cardsEarned": 1400,
                "battlesPlayed": 1,
                "wins": 1
            },
            {
                "tag": "9LL99RRU",
                "name": "Drareg",
                "cardsEarned": 1400,
                "battlesPlayed": 1,
                "wins": 1
            },
            {
                "tag": "88LQUVPG",
                "name": "Asten",
                "cardsEarned": 1400,
                "battlesPlayed": 1,
                "wins": 1
            },
            {
                "tag": "22Q2RCJVV",
                "name": "GaliciaNoMapa",
                "cardsEarned": 1400,
                "battlesPlayed": 1,
                "wins": 1
            },
            {
                "tag": "9CCLRG9Y",
                "name": "lebowsky",
                "cardsEarned": 1400,
                "battlesPlayed": 1,
                "wins": 1
            },
            {
                "tag": "YR8LLUU8",
                "name": "ELVIS",
                "cardsEarned": 1400,
                "battlesPlayed": 1,
                "wins": 1
            },
            {
                "tag": "2GQGUGQQ8",
                "name": "jmpv",
                "cardsEarned": 1400,
                "battlesPlayed": 1,
                "wins": 1
            },
            {
                "tag": "CRLRUQL",
                "name": "vrslm24",
                "cardsEarned": 1189,
                "battlesPlayed": 1,
                "wins": 1
            },
            {
                "tag": "2290RRUC9",
                "name": "Ivan",
                "cardsEarned": 1120,
                "battlesPlayed": 1,
                "wins": 1
            },
            {
                "tag": "8GV0Q9Q9",
                "name": "sergiomont yout",
                "cardsEarned": 1120,
                "battlesPlayed": 1,
                "wins": 1
            },
            {
                "tag": "2VC9GULRV",
                "name": "joker",
                "cardsEarned": 1120,
                "battlesPlayed": 1,
                "wins": 1
            },
            {
                "tag": "PYL8PPJ8",
                "name": "Ragnar",
                "cardsEarned": 1120,
                "battlesPlayed": 1,
                "wins": 1
            },
            {
                "tag": "UJPG9Q2J",
                "name": "Des",
                "cardsEarned": 1120,
                "battlesPlayed": 1,
                "wins": 1
            },
            {
                "tag": "8G8GCQRJ",
                "name": "Mikky",
                "cardsEarned": 1120,
                "battlesPlayed": 1,
                "wins": 1
            },
            {
                "tag": "RVV82QP",
                "name": "Guilliton",
                "cardsEarned": 1120,
                "battlesPlayed": 1,
                "wins": 1
            },
            {
                "tag": "LJVJ992Q",
                "name": "Jaaviix",
                "cardsEarned": 891,
                "battlesPlayed": 1,
                "wins": 1
            },
            {
                "tag": "298R9CCUL",
                "name": "floky",
                "cardsEarned": 840,
                "battlesPlayed": 1,
                "wins": 1
            },
            {
                "tag": "2LLYC9PPC",
                "name": "sifh21",
                "cardsEarned": 840,
                "battlesPlayed": 1,
                "wins": 1
            },
            {
                "tag": "8UVPJ8UR",
                "name": "Mikel",
                "cardsEarned": 840,
                "battlesPlayed": 1,
                "wins": 1
            },
            {
                "tag": "8GVJYQ8G",
                "name": "fruticola",
                "cardsEarned": 1680,
                "battlesPlayed": 1,
                "wins": 0
            },
            {
                "tag": "9LCY9Y2J",
                "name": "Mosquito23",
                "cardsEarned": 1680,
                "battlesPlayed": 1,
                "wins": 0
            },
            {
                "tag": "PYQC8C2G",
                "name": "Rayan",
                "cardsEarned": 1680,
                "battlesPlayed": 1,
                "wins": 0
            },
            {
                "tag": "JJP9JUG2",
                "name": "#Núñez",
                "cardsEarned": 1680,
                "battlesPlayed": 1,
                "wins": 0
            },
            {
                "tag": "88L9GPPR",
                "name": "♠♥♦♣BooooMZ♣♦♥♠",
                "cardsEarned": 1680,
                "battlesPlayed": 1,
                "wins": 0
            },
            {
                "tag": "G9J9CPL",
                "name": "S.J.P",
                "cardsEarned": 1487,
                "battlesPlayed": 1,
                "wins": 0
            },
            {
                "tag": "R9QRPYGL",
                "name": "KLOK",
                "cardsEarned": 1487,
                "battlesPlayed": 1,
                "wins": 0
            },
            {
                "tag": "CQ82YRR8",
                "name": "Adrián M",
                "cardsEarned": 1487,
                "battlesPlayed": 1,
                "wins": 0
            },
            {
                "tag": "Q0JUQLRR",
                "name": "kalico",
                "cardsEarned": 1400,
                "battlesPlayed": 1,
                "wins": 0
            },
            {
                "tag": "Y820L8LP",
                "name": "PuxiWeed",
                "cardsEarned": 1400,
                "battlesPlayed": 1,
                "wins": 0
            },
            {
                "tag": "GPCR09JL",
                "name": "★★★COROCOTA★★★",
                "cardsEarned": 1400,
                "battlesPlayed": 1,
                "wins": 0
            },
            {
                "tag": "2P89Q88G",
                "name": "SergileTTe",
                "cardsEarned": 1400,
                "battlesPlayed": 1,
                "wins": 0
            },
            {
                "tag": "2908VYGYV",
                "name": ":)DAVIDS:)",
                "cardsEarned": 1400,
                "battlesPlayed": 1,
                "wins": 0
            },
            {
                "tag": "VV9CRQ9U",
                "name": "MiriamBlade",
                "cardsEarned": 1400,
                "battlesPlayed": 1,
                "wins": 0
            },
            {
                "tag": "9UCPVYP",
                "name": "⚡CANTON⚡",
                "cardsEarned": 1400,
                "battlesPlayed": 1,
                "wins": 0
            },
            {
                "tag": "2Y22P9V28",
                "name": "⚽cristianO_vk⚽",
                "cardsEarned": 1400,
                "battlesPlayed": 1,
                "wins": 0
            },
            {
                "tag": "PYRL2CLU",
                "name": "ABEL",
                "cardsEarned": 1400,
                "battlesPlayed": 1,
                "wins": 0
            },
            {
                "tag": "208YYCQC",
                "name": "SergrogIX",
                "cardsEarned": 1400,
                "battlesPlayed": 1,
                "wins": 0
            },
            {
                "tag": "28GCCQ2L",
                "name": "maitee",
                "cardsEarned": 1400,
                "battlesPlayed": 1,
                "wins": 0
            },
            {
                "tag": "2CR82YPG",
                "name": "sodein14",
                "cardsEarned": 1400,
                "battlesPlayed": 1,
                "wins": 0
            },
            {
                "tag": "PVUVCPC2",
                "name": "Adrigm97",
                "cardsEarned": 1120,
                "battlesPlayed": 1,
                "wins": 0
            },
            {
                "tag": "U08RLC2U",
                "name": "Nayper",
                "cardsEarned": 840,
                "battlesPlayed": 1,
                "wins": 0
            },
            {
                "tag": "2PR0999J",
                "name": "SEIMUS",
                "cardsEarned": 840,
                "battlesPlayed": 1,
                "wins": 0
            }
        ],
        "standings": [
            {
                "tag": "RYRQ2L",
                "name": "愛媛JAPAN",
                "participants": 48,
                "battlesPlayed": 48,
                "wins": 31,
                "crowns": 48,
                "warTrophies": 2117,
                "warTrophiesChange": 131,
                "badge": {
                    "name": "flag_c_03",
                    "category": "02_Flag",
                    "id": 16000054,
                    "image": "https://royaleapi.github.io/cr-api-assets/badges/flag_c_03.png"
                }
            },
            {
                "tag": "2280QQU",
                "name": "愛知県JAPAN",
                "participants": 45,
                "battlesPlayed": 48,
                "wins": 28,
                "crowns": 43,
                "warTrophies": 2596,
                "warTrophiesChange": 78,
                "badge": {
                    "name": "Flame_01",
                    "category": "01_Symbol",
                    "id": 16000000,
                    "image": "https://royaleapi.github.io/cr-api-assets/badges/Flame_01.png"
                }
            },
            {
                "tag": "PPYPCV",
                "name": "INVERNALIA",
                "participants": 48,
                "battlesPlayed": 48,
                "wins": 25,
                "crowns": 48,
                "warTrophies": 2841,
                "warTrophiesChange": 0,
                "badge": {
                    "name": "Freeze_01",
                    "category": "01_Symbol",
                    "id": 16000168,
                    "image": "https://royaleapi.github.io/cr-api-assets/badges/Freeze_01.png"
                }
            },
            {
                "tag": "CPR2C9G",
                "name": "熱情幫（戰神天下）",
                "participants": 46,
                "battlesPlayed": 47,
                "wins": 25,
                "crowns": 42,
                "warTrophies": 2489,
                "warTrophiesChange": -25,
                "badge": {
                    "name": "A_Char_Barbarian_02",
                    "category": "03_Royale",
                    "id": 16000149,
                    "image": "https://royaleapi.github.io/cr-api-assets/badges/A_Char_Barbarian_02.png"
                }
            },
            {
                "tag": "YLG0RPJ",
                "name": "ADULT",
                "participants": 46,
                "battlesPlayed": 47,
                "wins": 21,
                "crowns": 45,
                "warTrophies": 2085,
                "warTrophiesChange": -79,
                "badge": {
                    "name": "Skull_05",
                    "category": "01_Symbol",
                    "id": 16000028,
                    "image": "https://royaleapi.github.io/cr-api-assets/badges/Skull_05.png"
                }
            }
        ],
        "seasonNumber": 9
    },
    {
        "createdDate": 1534338042,
        "participants": [
            {
                "tag": "2PU9202V",
                "name": "YourFkngGranpa",
                "cardsEarned": 1400,
                "battlesPlayed": 2,
                "wins": 2
            },
            {
                "tag": "88LQUVPG",
                "name": "Asten",
                "cardsEarned": 1120,
                "battlesPlayed": 2,
                "wins": 2
            },
            {
                "tag": "CRLRUQL",
                "name": "vrslm24",
                "cardsEarned": 1487,
                "battlesPlayed": 1,
                "wins": 1
            },
            {
                "tag": "298R9CCUL",
                "name": "floky",
                "cardsEarned": 1400,
                "battlesPlayed": 1,
                "wins": 1
            },
            {
                "tag": "QPCJQJQ",
                "name": "Jimlou",
                "cardsEarned": 1400,
                "battlesPlayed": 1,
                "wins": 1
            },
            {
                "tag": "GPCR09JL",
                "name": "★★★COROCOTA★★★",
                "cardsEarned": 1400,
                "battlesPlayed": 1,
                "wins": 1
            },
            {
                "tag": "8GVJYQ8G",
                "name": "fruticola",
                "cardsEarned": 1400,
                "battlesPlayed": 1,
                "wins": 1
            },
            {
                "tag": "2908VYGYV",
                "name": ":)DAVIDS:)",
                "cardsEarned": 1400,
                "battlesPlayed": 1,
                "wins": 1
            },
            {
                "tag": "R9QRPYGL",
                "name": "KLOK",
                "cardsEarned": 1400,
                "battlesPlayed": 1,
                "wins": 1
            },
            {
                "tag": "9UCPVYP",
                "name": "⚡CANTON⚡",
                "cardsEarned": 1400,
                "battlesPlayed": 1,
                "wins": 1
            },
            {
                "tag": "2PR0999J",
                "name": "SEIMUS",
                "cardsEarned": 1400,
                "battlesPlayed": 1,
                "wins": 1
            },
            {
                "tag": "2LLYC9PPC",
                "name": "sifh21",
                "cardsEarned": 1400,
                "battlesPlayed": 1,
                "wins": 1
            },
            {
                "tag": "CQ82YRR8",
                "name": "Adrián M",
                "cardsEarned": 1189,
                "battlesPlayed": 1,
                "wins": 1
            },
            {
                "tag": "LJVJ992Q",
                "name": "Jaaviix",
                "cardsEarned": 1189,
                "battlesPlayed": 1,
                "wins": 1
            },
            {
                "tag": "U08RLC2U",
                "name": "Nayper",
                "cardsEarned": 1120,
                "battlesPlayed": 1,
                "wins": 1
            },
            {
                "tag": "Y820L8LP",
                "name": "PuxiWeed",
                "cardsEarned": 1120,
                "battlesPlayed": 1,
                "wins": 1
            },
            {
                "tag": "YLL2YG0Q",
                "name": "SeRmAo",
                "cardsEarned": 1120,
                "battlesPlayed": 1,
                "wins": 1
            },
            {
                "tag": "9LL99RRU",
                "name": "Drareg",
                "cardsEarned": 1120,
                "battlesPlayed": 1,
                "wins": 1
            },
            {
                "tag": "PYQC8C2G",
                "name": "Rayan",
                "cardsEarned": 1120,
                "battlesPlayed": 1,
                "wins": 1
            },
            {
                "tag": "YR8LLUU8",
                "name": "ELVIS",
                "cardsEarned": 1120,
                "battlesPlayed": 1,
                "wins": 1
            },
            {
                "tag": "90GP0RQ9",
                "name": "vulosky",
                "cardsEarned": 1120,
                "battlesPlayed": 1,
                "wins": 1
            },
            {
                "tag": "PYL8PPJ8",
                "name": "Ragnar",
                "cardsEarned": 840,
                "battlesPlayed": 1,
                "wins": 1
            },
            {
                "tag": "UJPG9Q2J",
                "name": "Des",
                "cardsEarned": 280,
                "battlesPlayed": 1,
                "wins": 1
            },
            {
                "tag": "LG90P0RJ",
                "name": "Juan",
                "cardsEarned": 1785,
                "battlesPlayed": 1,
                "wins": 0
            },
            {
                "tag": "2P89Q88G",
                "name": "SergileTTe",
                "cardsEarned": 1680,
                "battlesPlayed": 1,
                "wins": 0
            },
            {
                "tag": "VV9CRQ9U",
                "name": "MiriamBlade",
                "cardsEarned": 1680,
                "battlesPlayed": 1,
                "wins": 0
            },
            {
                "tag": "2QJU9P2J",
                "name": "PABLOMIZZ",
                "cardsEarned": 1400,
                "battlesPlayed": 1,
                "wins": 0
            },
            {
                "tag": "2290RRUC9",
                "name": "Ivan",
                "cardsEarned": 1400,
                "battlesPlayed": 1,
                "wins": 0
            },
            {
                "tag": "2VC9GULRV",
                "name": "joker",
                "cardsEarned": 1400,
                "battlesPlayed": 1,
                "wins": 0
            },
            {
                "tag": "PVUVCPC2",
                "name": "Adrigm97",
                "cardsEarned": 1400,
                "battlesPlayed": 1,
                "wins": 0
            },
            {
                "tag": "28GCCQ2L",
                "name": "maitee",
                "cardsEarned": 1400,
                "battlesPlayed": 1,
                "wins": 0
            },
            {
                "tag": "8G8GCQRJ",
                "name": "Mikky",
                "cardsEarned": 1400,
                "battlesPlayed": 1,
                "wins": 0
            },
            {
                "tag": "2CR82YPG",
                "name": "sodein14",
                "cardsEarned": 1400,
                "battlesPlayed": 1,
                "wins": 0
            },
            {
                "tag": "Q9PJJYU8",
                "name": "SAID①⑦",
                "cardsEarned": 1189,
                "battlesPlayed": 1,
                "wins": 0
            },
            {
                "tag": "G9J9CPL",
                "name": "S.J.P",
                "cardsEarned": 1120,
                "battlesPlayed": 1,
                "wins": 0
            },
            {
                "tag": "8GV0Q9Q9",
                "name": "sergiomont yout",
                "cardsEarned": 1120,
                "battlesPlayed": 1,
                "wins": 0
            },
            {
                "tag": "22Q2RCJVV",
                "name": "GaliciaNoMapa",
                "cardsEarned": 1120,
                "battlesPlayed": 1,
                "wins": 0
            },
            {
                "tag": "88L9GPPR",
                "name": "♠♥♦♣BooooMZ♣♦♥♠",
                "cardsEarned": 1120,
                "battlesPlayed": 1,
                "wins": 0
            },
            {
                "tag": "RVV82QP",
                "name": "Guilliton",
                "cardsEarned": 1120,
                "battlesPlayed": 1,
                "wins": 0
            },
            {
                "tag": "8UVPJ8UR",
                "name": "Mikel",
                "cardsEarned": 1120,
                "battlesPlayed": 1,
                "wins": 0
            },
            {
                "tag": "208YYCQC",
                "name": "SergrogIX",
                "cardsEarned": 1120,
                "battlesPlayed": 1,
                "wins": 0
            },
            {
                "tag": "LPC0GYY",
                "name": "Golem de agua",
                "cardsEarned": 840,
                "battlesPlayed": 1,
                "wins": 0
            },
            {
                "tag": "PYRL2CLU",
                "name": "ABEL",
                "cardsEarned": 840,
                "battlesPlayed": 1,
                "wins": 0
            },
            {
                "tag": "9LCY9Y2J",
                "name": "Mosquito23",
                "cardsEarned": 840,
                "battlesPlayed": 1,
                "wins": 0
            },
            {
                "tag": "9CCLRG9Y",
                "name": "lebowsky",
                "cardsEarned": 840,
                "battlesPlayed": 1,
                "wins": 0
            },
            {
                "tag": "2QVQUCP",
                "name": "Maty97ro",
                "cardsEarned": 1400,
                "battlesPlayed": 0,
                "wins": 0
            }
        ],
        "standings": [
            {
                "tag": "8UVG98J",
                "name": "大连银的地儿",
                "participants": 48,
                "battlesPlayed": 48,
                "wins": 33,
                "crowns": 52,
                "warTrophies": 2246,
                "warTrophiesChange": 133,
                "badge": {
                    "name": "A_Char_King_03",
                    "category": "03_Royale",
                    "id": 16000146,
                    "image": "https://royaleapi.github.io/cr-api-assets/badges/A_Char_King_03.png"
                }
            },
            {
                "tag": "99028Q0",
                "name": "Level: Korea",
                "participants": 47,
                "battlesPlayed": 47,
                "wins": 29,
                "crowns": 46,
                "warTrophies": 2885,
                "warTrophiesChange": 79,
                "badge": {
                    "name": "flag_c_01",
                    "category": "02_Flag",
                    "id": 16000126,
                    "image": "https://royaleapi.github.io/cr-api-assets/badges/flag_c_01.png"
                }
            },
            {
                "tag": "PPYPCV",
                "name": "INVERNALIA",
                "participants": 46,
                "battlesPlayed": 47,
                "wins": 25,
                "crowns": 48,
                "warTrophies": 2841,
                "warTrophiesChange": 0,
                "badge": {
                    "name": "Freeze_01",
                    "category": "01_Symbol",
                    "id": 16000168,
                    "image": "https://royaleapi.github.io/cr-api-assets/badges/Freeze_01.png"
                }
            },
            {
                "tag": "8V9Y2UC",
                "name": "12machine",
                "participants": 45,
                "battlesPlayed": 48,
                "wins": 24,
                "crowns": 32,
                "warTrophies": 2653,
                "warTrophiesChange": -26,
                "badge": {
                    "name": "Star_Shine_03",
                    "category": "01_Symbol",
                    "id": 16000044,
                    "image": "https://royaleapi.github.io/cr-api-assets/badges/Star_Shine_03.png"
                }
            },
            {
                "tag": "9LYU80J0",
                "name": "Raisin Crystal",
                "participants": 45,
                "battlesPlayed": 46,
                "wins": 22,
                "crowns": 38,
                "warTrophies": 2174,
                "warTrophiesChange": -78,
                "badge": {
                    "name": "Star_Shine_02",
                    "category": "01_Symbol",
                    "id": 16000043,
                    "image": "https://royaleapi.github.io/cr-api-assets/badges/Star_Shine_02.png"
                }
            }
        ],
        "seasonNumber": 9
    },
    {
        "createdDate": 1534165136,
        "participants": [
            {
                "tag": "PYQC8C2G",
                "name": "Rayan",
                "cardsEarned": 1680,
                "battlesPlayed": 1,
                "wins": 1
            },
            {
                "tag": "LJVJ992Q",
                "name": "Jaaviix",
                "cardsEarned": 1680,
                "battlesPlayed": 1,
                "wins": 1
            },
            {
                "tag": "90GP0RQ9",
                "name": "vulosky",
                "cardsEarned": 1680,
                "battlesPlayed": 1,
                "wins": 1
            },
            {
                "tag": "CRLRUQL",
                "name": "vrslm24",
                "cardsEarned": 1487,
                "battlesPlayed": 1,
                "wins": 1
            },
            {
                "tag": "298R9CCUL",
                "name": "floky",
                "cardsEarned": 1400,
                "battlesPlayed": 1,
                "wins": 1
            },
            {
                "tag": "G9J9CPL",
                "name": "S.J.P",
                "cardsEarned": 1400,
                "battlesPlayed": 1,
                "wins": 1
            },
            {
                "tag": "PVUVCPC2",
                "name": "Adrigm97",
                "cardsEarned": 1400,
                "battlesPlayed": 1,
                "wins": 1
            },
            {
                "tag": "Q9PJJYU8",
                "name": "SAID①⑦",
                "cardsEarned": 1400,
                "battlesPlayed": 1,
                "wins": 1
            },
            {
                "tag": "2PU9202V",
                "name": "YourFkngGranpa",
                "cardsEarned": 1400,
                "battlesPlayed": 1,
                "wins": 1
            },
            {
                "tag": "2CR82YPG",
                "name": "sodein14",
                "cardsEarned": 1400,
                "battlesPlayed": 1,
                "wins": 1
            },
            {
                "tag": "2LLYC9PPC",
                "name": "sifh21",
                "cardsEarned": 1400,
                "battlesPlayed": 1,
                "wins": 1
            },
            {
                "tag": "YR8LLUU8",
                "name": "ELVIS",
                "cardsEarned": 1400,
                "battlesPlayed": 1,
                "wins": 1
            },
            {
                "tag": "LG90P0RJ",
                "name": "Juan",
                "cardsEarned": 1400,
                "battlesPlayed": 1,
                "wins": 1
            },
            {
                "tag": "2PR0999J",
                "name": "SEIMUS",
                "cardsEarned": 1400,
                "battlesPlayed": 1,
                "wins": 1
            },
            {
                "tag": "Q0JUQLRR",
                "name": "kalico",
                "cardsEarned": 1120,
                "battlesPlayed": 1,
                "wins": 1
            },
            {
                "tag": "2290RRUC9",
                "name": "Ivan",
                "cardsEarned": 1120,
                "battlesPlayed": 1,
                "wins": 1
            },
            {
                "tag": "Y820L8LP",
                "name": "PuxiWeed",
                "cardsEarned": 1120,
                "battlesPlayed": 1,
                "wins": 1
            },
            {
                "tag": "2QJU9P2J",
                "name": "PABLOMIZZ",
                "cardsEarned": 1120,
                "battlesPlayed": 1,
                "wins": 1
            },
            {
                "tag": "QPCJQJQ",
                "name": "Jimlou",
                "cardsEarned": 1120,
                "battlesPlayed": 1,
                "wins": 1
            },
            {
                "tag": "9UCPVYP",
                "name": "⚡CANTON⚡",
                "cardsEarned": 1120,
                "battlesPlayed": 1,
                "wins": 1
            },
            {
                "tag": "PYRL2CLU",
                "name": "ABEL",
                "cardsEarned": 1120,
                "battlesPlayed": 1,
                "wins": 1
            },
            {
                "tag": "9LCY9Y2J",
                "name": "Mosquito23",
                "cardsEarned": 1120,
                "battlesPlayed": 1,
                "wins": 1
            },
            {
                "tag": "22Q2RCJVV",
                "name": "GaliciaNoMapa",
                "cardsEarned": 1120,
                "battlesPlayed": 1,
                "wins": 1
            },
            {
                "tag": "9LL99RRU",
                "name": "Drareg",
                "cardsEarned": 1120,
                "battlesPlayed": 1,
                "wins": 1
            },
            {
                "tag": "8GVJYQ8G",
                "name": "fruticola",
                "cardsEarned": 1680,
                "battlesPlayed": 1,
                "wins": 0
            },
            {
                "tag": "VV9CRQ9U",
                "name": "MiriamBlade",
                "cardsEarned": 1680,
                "battlesPlayed": 1,
                "wins": 0
            },
            {
                "tag": "88L9GPPR",
                "name": "♠♥♦♣BooooMZ♣♦♥♠",
                "cardsEarned": 1680,
                "battlesPlayed": 1,
                "wins": 0
            },
            {
                "tag": "208YYCQC",
                "name": "SergrogIX",
                "cardsEarned": 1680,
                "battlesPlayed": 1,
                "wins": 0
            },
            {
                "tag": "LUPGGYQJ",
                "name": "daviid",
                "cardsEarned": 1680,
                "battlesPlayed": 1,
                "wins": 0
            },
            {
                "tag": "CQ82YRR8",
                "name": "Adrián M",
                "cardsEarned": 1487,
                "battlesPlayed": 1,
                "wins": 0
            },
            {
                "tag": "GPCR09JL",
                "name": "★★★COROCOTA★★★",
                "cardsEarned": 1400,
                "battlesPlayed": 1,
                "wins": 0
            },
            {
                "tag": "8GV0Q9Q9",
                "name": "sergiomont yout",
                "cardsEarned": 1400,
                "battlesPlayed": 1,
                "wins": 0
            },
            {
                "tag": "R9QRPYGL",
                "name": "KLOK",
                "cardsEarned": 1400,
                "battlesPlayed": 1,
                "wins": 0
            },
            {
                "tag": "PYL8PPJ8",
                "name": "Ragnar",
                "cardsEarned": 1400,
                "battlesPlayed": 1,
                "wins": 0
            },
            {
                "tag": "2908VYGYV",
                "name": ":)DAVIDS:)",
                "cardsEarned": 1400,
                "battlesPlayed": 1,
                "wins": 0
            },
            {
                "tag": "LPC0GYY",
                "name": "Golem de agua",
                "cardsEarned": 1400,
                "battlesPlayed": 1,
                "wins": 0
            },
            {
                "tag": "8UVPJ8UR",
                "name": "Mikel",
                "cardsEarned": 1400,
                "battlesPlayed": 1,
                "wins": 0
            },
            {
                "tag": "YLL2YG0Q",
                "name": "SeRmAo",
                "cardsEarned": 1120,
                "battlesPlayed": 1,
                "wins": 0
            },
            {
                "tag": "2P89Q88G",
                "name": "SergileTTe",
                "cardsEarned": 1120,
                "battlesPlayed": 1,
                "wins": 0
            },
            {
                "tag": "2VC9GULRV",
                "name": "joker",
                "cardsEarned": 1120,
                "battlesPlayed": 1,
                "wins": 0
            },
            {
                "tag": "88LQUVPG",
                "name": "Asten",
                "cardsEarned": 1120,
                "battlesPlayed": 1,
                "wins": 0
            },
            {
                "tag": "9CCLRG9Y",
                "name": "lebowsky",
                "cardsEarned": 1120,
                "battlesPlayed": 1,
                "wins": 0
            },
            {
                "tag": "28GCCQ2L",
                "name": "maitee",
                "cardsEarned": 1120,
                "battlesPlayed": 1,
                "wins": 0
            },
            {
                "tag": "RVV82QP",
                "name": "Guilliton",
                "cardsEarned": 1120,
                "battlesPlayed": 1,
                "wins": 0
            },
            {
                "tag": "2QVQUCP",
                "name": "Maty97ro",
                "cardsEarned": 560,
                "battlesPlayed": 1,
                "wins": 0
            },
            {
                "tag": "UJPG9Q2J",
                "name": "Des",
                "cardsEarned": 1120,
                "battlesPlayed": 0,
                "wins": 0
            },
            {
                "tag": "U08RLC2U",
                "name": "Nayper",
                "cardsEarned": 840,
                "battlesPlayed": 0,
                "wins": 0
            }
        ],
        "standings": [
            {
                "tag": "2VY2P8R2",
                "name": "#Team Negras",
                "participants": 45,
                "battlesPlayed": 42,
                "wins": 27,
                "crowns": 49,
                "warTrophies": 2562,
                "warTrophiesChange": 127,
                "badge": {
                    "name": "flag_j_03",
                    "category": "02_Flag",
                    "id": 16000077,
                    "image": "https://royaleapi.github.io/cr-api-assets/badges/flag_j_03.png"
                }
            },
            {
                "tag": "8GRURLQU",
                "name": "Alpha Male™ UK",
                "participants": 42,
                "battlesPlayed": 43,
                "wins": 26,
                "crowns": 49,
                "warTrophies": 2398,
                "warTrophiesChange": 76,
                "badge": {
                    "name": "Flame_04",
                    "category": "01_Symbol",
                    "id": 16000003,
                    "image": "https://royaleapi.github.io/cr-api-assets/badges/Flame_04.png"
                }
            },
            {
                "tag": "PPYPCV",
                "name": "INVERNALIA",
                "participants": 47,
                "battlesPlayed": 45,
                "wins": 24,
                "crowns": 50,
                "warTrophies": 2841,
                "warTrophiesChange": -1,
                "badge": {
                    "name": "Freeze_01",
                    "category": "01_Symbol",
                    "id": 16000168,
                    "image": "https://royaleapi.github.io/cr-api-assets/badges/Freeze_01.png"
                }
            },
            {
                "tag": "9YL0RGJ0",
                "name": "BestATK",
                "participants": 41,
                "battlesPlayed": 39,
                "wins": 18,
                "crowns": 35,
                "warTrophies": 1702,
                "warTrophiesChange": -32,
                "badge": {
                    "name": "Sword_01",
                    "category": "01_Symbol",
                    "id": 16000004,
                    "image": "https://royaleapi.github.io/cr-api-assets/badges/Sword_01.png"
                }
            },
            {
                "tag": "PPJ0LGG",
                "name": "allows dabbing",
                "participants": 39,
                "battlesPlayed": 42,
                "wins": 14,
                "crowns": 29,
                "warTrophies": 2448,
                "warTrophiesChange": -86,
                "badge": {
                    "name": "Flame_03",
                    "category": "01_Symbol",
                    "id": 16000002,
                    "image": "https://royaleapi.github.io/cr-api-assets/badges/Flame_03.png"
                }
            }
        ],
        "seasonNumber": 9
    }
]