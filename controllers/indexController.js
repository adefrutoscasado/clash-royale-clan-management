const asyncUtil = require('../services/asyncService');
const warlogService = require('../services/warlogService');
const clanService = require('../services/clanService');
const arrayService = require('../services/arrayService');
const playerService = require('../services/playerService');
const clashRoyaleApi = require('../api/clashRoyaleApi');
const clanTags = require('../config/clanTags');

function addPoints(n){
    var rx=  /(\d+)(\d{3})/;
    return String(n).replace(/^\d+/, function(w){
        while(rx.test(w)){
            w= w.replace(rx, '$1,$2');
        }
        return w;
    });
}

module.exports = {
    sendIndex: asyncUtil(async (req, res, next) => {
        let clanTag
        req.params.clanDivision ?
            clanTag = clanTags[(req.params.clanDivision).toLowerCase()] :
            clanTag = clanTags['a']

        let warlogResponse = await clashRoyaleApi.getWarlog(clanTag)
        let clanResponse = await clashRoyaleApi.getClan(clanTag)
        let members = clanService.getMembers(clanResponse)

        let lastUpdateTime = await clashRoyaleApi.getLastUpdateTime(clanTag)

        let warData = []
        for (const playerTag of members) {
            let warsPlayedByPlayerTag = warlogService.getWarsPlayedByPlayerTag(warlogResponse, playerTag)
            let warsWinsByPlayerTag = warlogService.getWarsWinsByPlayerTag(warlogResponse, playerTag)
            let warParticipations = warlogService.getWarsParticipationsByPlayerTag(warlogResponse, playerTag)
            let cardsEarnedByPlayerTag = warlogService.getCardsEarnedByPlayerTag(warlogResponse, playerTag)
            let nameByPlayerTag = clanService.getNameByTag(clanResponse, playerTag)
            let efectiveness = warlogService.calculateEffectivenessByPlayerTag(warlogResponse, playerTag)
            warData.push({
                playerTag,
                'name': nameByPlayerTag,
                'warsPlayed': warsPlayedByPlayerTag,
                'warsWins': warsWinsByPlayerTag,
                'warsWinsRatio': (((warsWinsByPlayerTag / warsPlayedByPlayerTag) || 0) * 100).toFixed(2),
                'cardsEarned': cardsEarnedByPlayerTag,
                'warsParticipations': warParticipations,
                'cardsEarnedRatio': ((cardsEarnedByPlayerTag / warParticipations || 0)).toFixed(2) || 0,
                'efectiveness':  efectiveness.toFixed(3)
            })
        }

        let playerData = []
        for (const playerTag of members) {

            let nameByPlayerTag = clanService.getNameByTag(clanResponse, playerTag)
            let playerResponse = await clashRoyaleApi.getPlayer(playerTag)
            let cardCommonMean = playerService.getMeanLevelByRarity(playerResponse, 'Common')
            let cardCommonVariance = playerService.getVarianceLevelByRarity(playerResponse, 'Common')
            let cardRareMean = playerService.getMeanLevelByRarity(playerResponse, 'Rare')
            let cardRareVariance = playerService.getVarianceLevelByRarity(playerResponse, 'Rare')
            let cardEpicMean = playerService.getMeanLevelByRarity(playerResponse, 'Epic')
            let cardEpicVariance = playerService.getVarianceLevelByRarity(playerResponse, 'Epic')
            let cardLegendaryMean = playerService.getMeanLevelByRarity(playerResponse, 'Legendary')
            let cardLegendaryVariance = playerService.getVarianceLevelByRarity(playerResponse, 'Legendary')
            let cardGoldSpent = playerService.getCardsGoldSpent(playerResponse)
            let deckLink = playerService.getDeckLink(playerResponse)
            let currentDeck = playerService.getCurrentDeck(playerResponse)
            let totalGames = playerService.getTotalGames(playerResponse)
            let winsGames = playerService.getWinsGames(playerResponse)
            let lossesGames = playerService.getLossesGames(playerResponse)
            let drawsGames = playerService.getDrawsGames(playerResponse)
            let winsPercentGames = playerService.getWinsPercentGames(playerResponse)
            let lossesPercentGames = playerService.getLossesPercentGames(playerResponse)
            let drawsPercentGames = playerService.getDrawsPercentGames(playerResponse)
            let threeCrownWinsStats = playerService.getThreeCrownWinsStats(playerResponse)
            let timeSpentPlaying = playerService.getTimeSpentPlaying(playerResponse)
            let threeCrownWinsRatio = playerService.getThreeCrownWinsRatio(playerResponse)

            playerData.push({
                playerTag,
                'name': nameByPlayerTag,
                'cardCommonMean': cardCommonMean.toFixed(2),
                'cardCommonVariance': cardCommonVariance.toFixed(2),
                'cardRareMean': cardRareMean.toFixed(2),
                'cardRareVariance': cardRareVariance.toFixed(2),
                'cardEpicMean': cardEpicMean.toFixed(2),
                'cardEpicVariance': cardEpicVariance.toFixed(2),
                'cardLegendaryMean': cardLegendaryMean.toFixed(2),
                'cardLegendaryVariance': cardLegendaryVariance.toFixed(2),
                'cardGoldSpent': addPoints(cardGoldSpent),
                deckLink,
                currentDeck,
                totalGames,
                winsGames,
                lossesGames,
                drawsGames,
                winsPercentGames,
                lossesPercentGames,
                drawsPercentGames,
                threeCrownWinsStats,
                timeSpentPlaying,
                threeCrownWinsRatio: threeCrownWinsRatio.toFixed(2),
            })
            // console.log(JSON.stringify(playerData));
        }

        res.render('index', {
            warData,
            playerData,
            env: process.env.NODE_ENV,
            lastUpdateTime
        })
    })
}