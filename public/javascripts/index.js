$(function() {
    // add new widget called indexFirstColumn
    $.tablesorter.addWidget({
        // give the widget a id
        id: "indexFirstColumn",
        // format is called when the on init and when a sorting has finished
        format: function(table) {               
            // loop all tr elements and set the value for the first column  
            for(let i=0; i < table.tBodies[0].rows.length; i++) {
                $("tbody tr:eq(" + i + ") td:first",table).html(i+1);
            }                                   
        }
    });
    $("table").tablesorter({
        widgets: ['indexFirstColumn'],
        // Disable sorting of first column
        headers: {
            0: { sorter: false }
        },
    });
});

$(document).ready(function() { 
    // call the tablesorter plugin 
    $(".warstats").trigger("sorton", [ [[9,1]] ]);
    $(".winsStats").trigger("sorton", [ [[11,1]] ]);
    $(".cardstats").trigger("sorton", [ [[10,1]] ]);
}); 

function GetLargestValueForColumn(table) {
        let colCount = $('table:eq('+ table +') th').length;
        let rowCount = $('table:eq('+ table +') tbody tr').length;
        let largestVals = new Array();
        let lowestVals = new Array();

        for (let c = 0; c < colCount; c++) {
            let values = new Array();
            for (let r = 0; r < rowCount; r++) {
                let value = $('table:eq('+ table +') tbody tr:eq(' + r + ') td:eq(' + c + ')').text();
                value = value.replace("%", "").replace("kg", "").replace(" ", "").replace(/\./g,'').replace(/\,/g,'').replace(/\:/g,'');
                // console.log($('tbody tr:eq(' + r + ') td:eq(' + c + ')').text(), value)
                values.push(value);
            }
            // console.log(values)
            let largest = Math.max.apply(Math, values);
            let lowest = Math.min.apply(Math, values);
            largestVals.push(largest);
            lowestVals.push(lowest)

            $('tbody tr').each(function() {
                let text = $(this).find('td:eq(' + c + ')').text();
                text = text.replace("%", "").replace("kg", "").replace(" ", "").replace(/\./g,'').replace(/\,/g,'').replace(/\:/g,'');
                if (text == largest) {
                    $(this).find('td:eq(' + c + ')').addClass("max");
                }
                if (text == lowest) {
                    $(this).find('td:eq(' + c + ')').addClass("min");
                }
            });
        }
    return
}
$(function() {
    $('table').each(function(table) {GetLargestValueForColumn(table)});
})


$(window).on('load',function() {
    $('body').fadeIn(1000);
});

