const axios = require('axios');
const { cacheAdapterEnhancer } = require('axios-extensions');
const moment = require('moment');
const CONSTANTS = require('../constants/constants');
const sampleResponse = require('../debug/IndexResponse');
const LRUCache = require("lru-cache")

let debug = false

CLASH_ROYALE_TOKEN = CONSTANTS.CLASH_ROYALE_TOKEN

const options = {
    defaultCache: new LRUCache({ maxAge: 60 * 60 * 1000, max: 100 })
}

const http = axios.create({
        headers: { 'Cache-Control': 'no-cache' },
        timeout: 60000,
        // cache will be enabled by default
        adapter: cacheAdapterEnhancer(axios.defaults.adapter)
    }
)

let config = {
    headers: {
        Authorization: `Bearer ${CLASH_ROYALE_TOKEN}`,
    }
}

const clashRoyaleBaseUrl = 'https://api.royaleapi.com'

getWarlogUrl = (clanTag) => `${clashRoyaleBaseUrl}/clan/${clanTag}/warlog`
getClanUrl = (clanTag) => `${clashRoyaleBaseUrl}/clan/${clanTag}`
getPlayerUrl = (playerTag) => `${clashRoyaleBaseUrl}/player/${playerTag}`

module.exports={
    getWarlog: async (clanTag, forceUpdate = false) => {
        config['forceUpdate'] = forceUpdate
        console.log(moment())
        if (debug) {
            console.warn('debugResponse')
            return sampleResponse.warlogResponse
        }
        try {
            const response = await http.get(getWarlogUrl(clanTag), config)
            console.log(moment())
            const data = response.data
            data.forEach(element => {
                element['createdDateISO'] = moment.unix(element.createdDate).toISOString()
            })
            return data
        } catch (error) {
            console.log(error)
        }
    },
    getClan: async (clanTag, forceUpdate = false) => {
        config['forceUpdate'] = forceUpdate
        if (debug) {
            console.warn('debugResponse')
            return sampleResponse.clanResponse
        }
        console.log(moment())
        try {
            const response = await http.get(getClanUrl(clanTag), config)
            console.log(moment())
            const data = response.data
            return data
        } catch (error) {
            console.log(error)
        }
    },
    getPlayer: async (playerTag, forceUpdate = false) => {
        config['forceUpdate'] = forceUpdate
        if (debug) {
            console.warn('debugResponse')
            return sampleResponse.playerResponse
        }
        console.log(moment())
        try {
            const response = await http.get(getPlayerUrl(playerTag), config)
            console.log(moment())
            const data = response.data
            return data
        } catch (error) {
            console.log(error)
        }
    }
}
