'use strict'

const arrayService = require('../services/arrayService');

const self = {
    getParticipants: (warlogResponse) => {
        let participants = []
        warlogResponse.forEach(el => {
            el.participants.forEach(participant => {
                participants.push(participant.tag)
            })
        })
        return arrayService.removeDuplicateEntries(participants)
    },
    getWarsPlayedByPlayerTag: (warlogResponse, playerTag) => {
        let warsPlayed = 0
        warlogResponse.forEach(el => {
            el.participants.forEach(participant => {
                if (participant && participant.tag === playerTag) warsPlayed += participant.battlesPlayed
            })
        })
        return warsPlayed
    },
    getWarsWinsByPlayerTag: (warlogResponse, playerTag) => {
        let warsWins = 0
        warlogResponse.forEach(el => {
            el.participants.forEach(participant => {
                if (participant && participant.tag === playerTag) warsWins += participant.wins
            })
        })
        return warsWins
    },
    getCardsEarnedByPlayerTag: (warlogResponse, playerTag) => {
        let cardsEarned = 0
        warlogResponse.forEach(el => {
            el.participants.forEach(participant => {
                if (participant && participant.tag === playerTag) cardsEarned += participant.cardsEarned
            })
        })
        return cardsEarned
    },
    getCardsEarnedByPlayerTag: (warlogResponse, playerTag) => {
        let cardsEarned = 0
        warlogResponse.forEach(el => {
            el.participants.forEach(participant => {
                if (participant && participant.tag === playerTag) cardsEarned += participant.cardsEarned
            })
        })
        return cardsEarned
    },
    getWarsParticipationsByPlayerTag: (warlogResponse, playerTag) => {
        let warsParticipations = 0
        warlogResponse.forEach(el => {
            el.participants.forEach(participant => {
                if (participant && participant.tag === playerTag) warsParticipations += 1
            })
        })
        return warsParticipations
    },
    getWarsPlayedByClan: (warlogResponse) => {
        let warsPlayed = 0
        warlogResponse.forEach(el => {
            el.participants.forEach(participant => {
                warsPlayed += Number(participant.battlesPlayed)
            })
        })
        return warsPlayed
    },
    getWarsWinsByClan: (warlogResponse) => {
        let warsWins = 0
        warlogResponse.forEach(el => {
            el.participants.forEach(participant => {
                warsWins += Number(participant.wins)
            })
        })
        return warsWins
    },
    calculateEffectivenessByPlayerTag: (warlogResponse, playerTag) => {
        let totalWarsWins =  self.getWarsWinsByClan(warlogResponse)
        let totalWarsPlayed =  self.getWarsPlayedByClan(warlogResponse)
        let warsWinsByPlayer = self.getWarsWinsByPlayerTag(warlogResponse, playerTag)
        let warsPlayedByPlayer = self.getWarsPlayedByPlayerTag(warlogResponse, playerTag)
        let totalClanMean = totalWarsWins / totalWarsPlayed
        let base
        warsPlayedByPlayer > 10 ? base = warsPlayedByPlayer : base = 10
        let efectiveness = (warsWinsByPlayer + ((base - warsPlayedByPlayer)*totalClanMean))/base
        // console.log({warsWinsByPlayer, warsPlayedByPlayer, totalClanMean, base, efectiveness})
        return efectiveness
    }
}

module.exports = self